<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="{{asset('images/web/logo/logo.svg')}}" />
        <title inertia>{{ config('app.name', 'Mytessuti') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}?v=1">
        <link rel="stylesheet" href="{{asset("fonts/icomoon/style.css")}}">
        <!-- Scripts -->
        @routes
        <script src="{{ mix('js/app.js') }}?v=7" defer></script>
    </head>
    <body class="font-sans antialiased">
    @include('admin.preloder')
        @inertia
    </body>
</html>
