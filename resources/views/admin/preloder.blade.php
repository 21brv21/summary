<style>

    /* Preloder */

    #preloder {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 999999;
        align-items: center;
        justify-content: center;
    }

    .loader {
        display: inline-block;
        width: 80px;
        height: 80px;
        margin: auto;
    }

    .loader:after {
        content: " ";
        display: block;
        width: 40px;
        height: 40px;
        margin: 8px;
        border-radius: 50%;
        border: 6px solid #86639d;
        border-color: #86639d transparent #09c6ab transparent;
        animation: loader 1.2s linear infinite;
    }


    @keyframes loader {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }


</style>

<div id="preloder" style="display: none;">
    <div class="loader"></div>
</div>
