@extends('web.layouts.app')

@section('seo_title', $blog->translation->seo_title)
@section('seo_description', $blog->translation->seo_description)

@section('seoData')
    @foreach($seoSchemas as $key => $seoSchema)<script type="application/ld+json" data-seo="{{$key}}">{!! $seoSchema !!}</script>@endforeach
@endsection

@section('content')
    <section class="pageText m-t-50 m-b-30 m-b-15">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <p class="h2 textCenter sectionTitle m-b-40" style="text-align: center">{{$blog->translation->name}}</p>
                    <img src="{{$blog->image}}" class="imageArticle">
                    {!! $blog->translation->content !!}
                </div>
            </div>
        </div>
    </section>
    @include('web.layouts.map', ['slug' => $blog->slug])
@endsection
