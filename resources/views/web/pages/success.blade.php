@extends('web.layouts.app')

@section('seo_title', $page->translation->seo_title)
@section('seo_description', $page->translation->seo_description)

@section('seoData')
    @foreach($seoSchemas as $key => $seoSchema)<script type="application/ld+json" data-seo="{{$key}}">{!! $seoSchema !!}</script>@endforeach
@endsection

@section('content')

    <section class="mainSection">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-t-50 p-b-50 lineText">
                    <h1 class="h4 m-b-30">{{trans('crud.pageSuccessText')}}</h1>
                    <p class="successPage">
                        {{trans('crud.pageSuccessQuestion')}} <br>
                        <a href="tel:{{getSetting('phone_kievstar')}}" class="globalPhone colMain">
                            <i class="fas fa-phone"></i> {{getSetting('phone_kievstar')}}
                        </a>
                        <a href="tel:{{getSetting('phone_vodafone')}}" class="globalPhone colMain">
                            <i class="fas fa-phone"></i> {{getSetting('phone_vodafone')}}
                        </a>
                        <a href="tel:{{getSetting('phone_life')}}" class="globalPhone colMain">
                            <i class="fas fa-phone"></i> {{getSetting('phone_life')}}
                        </a>
                        <a href="tel:{{getSetting('phone_city')}}" class="globalPhone colMain">
                            <i class="fas fa-phone"></i> {{getSetting('phone_city')}}
                        </a>
                    </p>
                    <p>{{trans('crud.pageSuccessThanks')}}</p>
                </div>
            </div>
        </div>
    </section>
@endsection
