<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('web.layouts.googletagmanager')
    <title>{{$page->translation->seo_title}}</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="msapplication-TileColor" content="black">
    <meta name="theme-color" content="black">

    <meta name="description" content="{{$page->translation->seo_description}}">
{{--    <meta name="robots" content="noindex,nofollow">--}}

    @foreach($seoSchemas as $key => $seoSchema)<script type="application/ld+json" data-seo="{{$key}}">{!! $seoSchema !!}</script>@endforeach

    <meta property="og:type" content="website">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:title" content="{{$page->translation->seo_title}}">
    <meta property="og:image" content="{{asset('/images/web/logo/logo.svg')}}">
    <meta property="og:description" content="{{$page->translation->seo_description}}">
    <meta property="og:site_name" content="{{config('app.name')}}">
    <meta property="og:locale" content="{{app()->getLocale()}}">

    @routes
    @include('web.layouts.style')
</head>
<body>
@include('web.layouts.googletagmanager2')
@include('web.layouts.preloder')
<div id="app">
    @include('web.layouts.header')
    <div class="wrapper" id="content">
            <div class="site-wrap" id="app">
                <div class="main-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <p class="text-uppercase h1">{{trans('web.home.title')}}</p>
                                <p class="text-uppercase">{{trans('web.home.subtitle')}}</p>
                                <button
                                    onclick="document.getElementById('contactBtn').click();"
                                    class="ecommerce ecommerceSelectItemClick ecommerceViewItemClick btn btn-primary"
                                    id="ecommerceFirst"
                                    data-ecommerceName="{{trans('web.orderForm.water')}}"
                                    data-ecommercePrice="{{getSetting('price_first')}}"
                                    data-ecommerceList="First"
                                >
                                    {{trans('web.home.title_btn')}}
                                </button>
                            </div>
                        </div>
                        <div class="ellipse">
                            @include('web.svg.ellipse')
                            <img src="{{asset('images/web/image_4.webp')}}">
                        </div>
                        <div class="butl text-end">
                            <img src="{{asset('images/web/water/butl-3.webp')}}">
                            <img src="{{asset('images/web/water/butl-3.webp')}}">
                        </div>
                    </div>
                </div>
                <div class="action-section">
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-md-3 col-sm-2 action-image">
                                <img src="{{asset('images/web/water/butl_3_present.webp')}}">
                            </div>
                            <div class="col-md-9 col-sm-10 action-content text-center">
                                <h2 class="text-uppercase">{{trans('web.home.action')}}</h2>
                                <p class="p1 text-uppercase">{{trans('web.home.action_description')}}</p>
                                <p class="p2 text-uppercase">{{trans('web.home.action_free')}}</p>
                                <hr>
                                <p class="p3">{{trans('web.home.action_text')}} = {{getSetting('sale_price_first') * 2}} {{trans('web.grn')}}</p>
                                <button
                                    onclick="document.getElementById('contactBtn').click();"
                                    class="ecommerce ecommerceSelectItemClick ecommerceViewItemClick btn btn-primary"
                                    id="ecommercePromotion"
                                    data-ecommerceName="{{trans('web.orderForm.water')}}"
                                    data-ecommercePrice="{{getSetting('sale_price_first')}}"
                                    data-ecommerceList="Promotion"
                                >
                                    {{trans('web.home.action_btn')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h2 class="text-uppercase">{{trans('web.home.get_title')}}</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-3 text-end">
                                <div class="order-num">
                                    <p class="num">1</p>
                                    <svg width="113" height="151" viewBox="0 0 113 151" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#order"></use>
                                    </svg>
                                    <div class="order-line d-none d-sm-block">
                                        <svg width="3" height="69" viewBox="0 0 3 69" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#order-line"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-9">
                                <p class="h3 text-uppercase">{{trans('web.home.get_first_title')}}</p>
                                <div class="row">
                                    <div class="col-12 col-sm-7">
                                        <p>{{trans('web.get.first_1')}}</p>
                                        <p>{{trans('web.get.first_2')}}</p>
                                        <p>{{trans('web.get.first_3')}}</p>
                                    </div>
                                    <div class="col-12 col-sm-5">
                                        <div class="social">
                                            <a href="{{getSetting('viber')}}"><i class="fab fa-viber"></i></a>
                                            <a href="{{getSetting('telegram')}}"><i class="fab fa-telegram"></i></a>
                                        </div>
                                        <p>{{trans('web.get.time')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-3 text-end">
                                <div class="order-num">
                                    <p class="num">2</p>
                                    <svg width="113" height="151" viewBox="0 0 113 151" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#order"></use>
                                    </svg>
                                    <div class="order-line d-none d-sm-block">
                                        <svg width="3" height="69" viewBox="0 0 3 69" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#order-line"></use>
                                        </svg>

                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-9">
                                <p class="h3 text-uppercase">{{trans('web.get.second_2')}}</p>
                                <div class="w100 stageIcon">
                                    <p><i class="fas fa-money-bill"></i> {{trans('web.get.cash')}}</p>
                                    <p><i class="fa fa-credit-card"></i> {{trans('web.get.online')}}</p>
                                    <p><i class="fa fa-bank"></i> {{trans('web.get.bank')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-3 text-end">
                                <div class="order-num">
                                    <p class="num">3</p>
                                    <svg width="113" height="151" viewBox="0 0 113 151" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#order"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="col-12 col-sm-9">
                                <p class="h3 text-uppercase">{{trans('web.home.get_third_title')}}</p>
                                <p>{{trans('web.get.first_1')}}</p>
                                <p>{{trans('web.get.first_2')}}</p>
                                <p>{{trans('web.get.first_3')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order_2">
                    <div class="container">
                        <div class="row text-center justify-content-center">
                            <div class="col-lg-4 col-md-6">
                                <button onclick="document.getElementById('contactBtn').click();"
                                        class="btn btn-primary ecommerce ecommerceSelectItemClick ecommerceViewItemClick"
                                        id="ecommerceSteps"
                                        data-ecommerceName="{{trans('web.orderForm.water')}}"
                                        data-ecommercePrice="{{getSetting('price_first')}}"
                                        data-ecommerceList="Steps"
                                >
                                    {{trans('web.order')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="stage">
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-12">
                                <h2 class="text-uppercase">{{trans('web.home.icons_title')}}</h2>
                                <p class="h3 text-uppercase">{{trans('web.home.icons_subtitle')}}</p>
                                <p class="text-muted">{{trans('web.home.icons_text')}}</p>
                                <p>{{trans('web.home.icons_description')}}</p>
                                <div class="stage-block">
                                    <div class="stage-1">
                                        @include('web.svg.iconFirst')
                                        <p>{{trans('web.home.icons_third')}}</p>
                                    </div>
                                    <div class="stage-2">
                                        @include('web.svg.iconSecond')
                                        <p>{{trans('web.home.icons_second')}}</p>
                                    </div>
                                    <div class="stage-3">
                                        @include('web.svg.iconThird')
                                        <p>{{trans('web.home.icons_first')}}</p>
                                    </div>
                                    <div class="stage-4">
                                        @include('web.svg.iconFour')
                                        <p>{{trans('web.home.icons_four')}}</p>
                                    </div>
                                    <div class="stage-5">
                                        @include('web.svg.iconFive')
                                        <p>{{trans('web.home.icons_five')}}</p>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="logo-block col-12 col-md-4 d-none d-md-flex">
                                            <img src="{{asset('images/web/logo/logo.webp')}}"/>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="btn-block col-12 col-md-4">
                                            <a href="{{$certificate}}" class="btn btn-outline-primary" target="_blank" rel="nofollow">
                                                {{trans('web.home.icons_more')}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('web.layouts.assortment', ['products' => getProducts()])
                @include('web.layouts.map')
                <div class="ordering">
                    <div class="container">
                        <div class="row">
                            <order-form :id="{{json_encode('order')}}"
                                        :products="{{json_encode(getProducts())}}"
                                        :outpost-price="{{json_encode(getSetting('outpost_price'))}}"
                                        :price-first="{{json_encode(getSetting('price_first'))}}"
                                        :price-second="{{json_encode(getSetting('price_second'))}}"
                                        :price-third="{{json_encode(getSetting('price_third'))}}"
                                        :sale-price-first="{{json_encode(getSetting('sale_price_first'))}}"
                                        :sale-price-second="{{json_encode(getSetting('sale_price_second'))}}"
                                        :sale-price-third="{{json_encode(getSetting('sale_price_third'))}}"
                            ></order-form>
                            <div class="col-12 col-lg-6">
                                <div class="ordering-text">
                                    <p>{!! trans('web.home.text_0') !!}</p>
                                    <p>{!! trans('web.home.text_1') !!}</p>
                                    <p>{!! trans('web.home.text_2') !!}</p>
                                    <p>{!! trans('web.home.text_3') !!}</p>
                                    <p></p>
                                    <p></p>
                                    <p>{{trans('web.home.text_4')}}</p>
                                    <p></p>
                                    <p><strong>{{trans('web.home.text_5')}}</strong></p>
                                    <p>{{trans('web.home.text_6')}}</p>
                                    <p>{{trans('web.home.text_7')}} {{getSetting('outpost_price')}} {{trans('web.grn')}}.</p>
                                    <p>{{trans('web.home.text_8')}}</p>
                                    <p>{{trans('web.home.text_9')}}</p>
                                    <p></p>
                                    <p><strong>{{trans('web.home.text_10')}}</strong></p>
                                    <p>{{trans('web.home.text_11')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="articles pageText">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                {!! $page->translation->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    @include('web.svg.drop')
    @include('web.layouts.footer')

    <div class="displayCenterFlex" id="notification">
        <p></p>
    </div>

    @include('web.layouts.modal')
    <ecommerce-events :products="{{json_encode(getProducts())}}"
                      :outpost-price="{{json_encode(getSetting('outpost_price'))}}"
                      :price-first="{{json_encode(getSetting('price_first'))}}"
                      :price-second="{{json_encode(getSetting('price_second'))}}"
                      :price-third="{{json_encode(getSetting('price_third'))}}"
                      :sale-price-first="{{json_encode(getSetting('sale_price_first'))}}"
                      :sale-price-second="{{json_encode(getSetting('sale_price_second'))}}"
                      :sale-price-third="{{json_encode(getSetting('sale_price_third'))}}"
    ></ecommerce-events>
</div>

@include('web.layouts.scripts')
@stack('scripts')
</body>
</html>
