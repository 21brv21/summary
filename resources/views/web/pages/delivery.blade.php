@extends('web.layouts.app')

@section('seo_title', $page->translation->seo_title)
@section('seo_description', $page->translation->seo_description)

@section('seoData')
    @foreach($seoSchemas as $key => $seoSchema)<script type="application/ld+json" data-seo="{{$key}}">{!! $seoSchema !!}</script>@endforeach
@endsection

@section('content')

    <section class="pageText m-t-50 m-b-30 m-b-15">
        <div class="container">
            <div class="row">
                <div class="col-md-12 m-b-30">
                    {!! $page->translation->description !!}
                </div>
            </div>
        </div>
    </section>
    @include('web.layouts.map')
@endsection
