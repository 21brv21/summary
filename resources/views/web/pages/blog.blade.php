@extends('web.layouts.app')

@section('seo_title', $page->translation->seo_title)
@section('seo_description', $page->translation->seo_description)
@section('seo_image', $page->image ?? '/images/web/log/logo.png')

@section('seoData')
    @foreach($seoSchemas as $key => $seoSchema)
        <script type="application/ld+json" data-seo="{{$key}}">{!! $seoSchema !!}</script>@endforeach
@endsection

@section('content')

    <section class="ecommerce blogSection m-t-50 m-b-30 m-b-15"
             id="ecommerceCatalog"
             data-ecommerceName="{{trans('web.orderForm.water')}}"
             data-ecommercePrice="{{getSetting('price_first')}}"
             data-ecommerceList="First"
    >
        <div class="container">
            <div class="row">
                <div class="col-md-12 m-b-15">
                    <h1 class="h2 textCenter sectionTitle m-b-40">{{$page->translation->name}}</h1>
                </div>
            </div>
            @foreach($articles as $article)
                <div class="row boxBlog">
                    <div class="col-12 col-md-5">
                        <div class="thumb">
                            <div class="image">
                                <a href="{{route('web.slug', ['slug' => $article->slug])}}">
                                    <img src="{{$article->image}}" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 articleBox">
                        <div class="article-content">
                            <p class="h4">
                                <a href="{{route('web.slug', ['slug' => $article->slug])}}">{{$article->translation->name}}</a>
                            </p>
                            <p>{{$article->translation->short_description}}</p>
                            <a href="{{route('web.slug', ['slug' => $article->slug])}}"
                               class="btn btn-primary">{{trans('web.details')}}</a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col-md-12 links displayCenterFlex">
                    {{ $articles->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
