<div class="delivery">
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <h2 class="text-uppercase">{{trans('web.assortment')}}</h2>
            </div>
        </div>
        <div class="row text-center text-uppercase">
            @foreach($products as $product)
                <div class="ecommerce layout col-12 col-lg-4"
                     id="assortment{{$product->id}}"
                     data-ecommerceName="{{$product->name}}"
                     data-ecommercePrice="{{$product->price}}"
                     data-ecommerceList="Assortment"
                >
                    <div class="thumb">
                        <div class="image">
                            <a href="{{route('web.slug', ['slug' => $product->slug])}}">
                                <img src="{{$product->image}}"
                                     class="img-responsive ecommerceSelectItemClick"
                                     data-ecommerceName="{{$product->name}}"
                                     data-ecommercePrice="{{$product->price}}"
                                     data-ecommerceList="Assortment"
                                >
                            </a>
                        </div>
                        <div class="caption">
                            <p class="h4">
                                <a href="{{route('web.slug', ['slug' => $product->slug])}}">
                                    <span
                                        class="img-responsive ecommerceSelectItemClick"
                                        data-ecommerceName="{{$product->name}}"
                                        data-ecommercePrice="{{$product->price}}"
                                        data-ecommerceList="Assortment"
                                    >{{$product->name}}
                                    </span>
                                </a>
                            </p>
                            <p class="price">{{intval($product->price)}} {{trans('web.grn')}}</p>
                        </div>
                        <div class="button-group">
                            <button class="ecommerceViewItemClick btn btn-primary w100"
                                    onclick="document.getElementById('contactBtn').click();"
                                    data-ecommerceName="{{$product->name}}"
                                    data-ecommercePrice="{{$product->price}}"
                                    data-ecommerceList="Assortment"
                            >
                                {{trans('web.buy')}}
                            </button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
