<header class="navbar-main text-uppercase" role="banner">
    <div class="container">
        <div class="row d-none d-md-flex">
            <div class="col-6 text-start">
                <div class="header-social">
                    <a href="{{getSetting('viber')}}"><i class="fab fa-viber"></i></a>
                    <a href="{{getSetting('telegram')}}"><i class="fas fa-paper-plane"></i></a>
                </div>
            </div>
            <div class="col-6 text-end">
                <ul class="list-inline lang">
                    <li class="list-inline-item">
                        <a @if(app()->getLocale() === 'uk') class="active" @endif href="{{route('web.translate', ['lang' => 'uk'])}}">UK</a>
                    </li>
                    <li class="list-inline-item">
                        <a @if(app()->getLocale() === 'en') class="active" @endif href="{{route('web.translate', ['lang' => 'en'])}}">EN</a>
                    </li>
                    <li class="list-inline-item">
                        <a @if(app()->getLocale() === 'ru') class="active" @endif href="{{route('web.translate', ['lang' => 'ru'])}}">RU</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="container" id="navigation">
        <div class="row">
            <div class="col-12 col-md-2 col-lg-2 header-logo">
                <a href="/">
                    <img src="{{asset('images/web/logo/logo.webp')}}" id="logo">
                </a>
            </div>
            <div class="col-md-7 col-lg-7 d-none d-md-block text-center">
                <nav class="navigation-menu" role="navigation">
                    <ul class="main-menu list-unstyled list-inline">
                        <li class="list-inline-item">
                            <a @if(request()->route()->getName() === 'web.home') class="active" @endif href="{{route('web.home')}}"><span>{{trans('web.menu.home')}}</span></a>
                        </li>
                        <li class="list-inline-item">
                            <a @if(request()->route()->getName() === 'web.payment') class="active" @endif href="{{route('web.payment')}}"><span>{{trans('web.menu.payment')}}</span></a>
                        </li>
                        <li class="list-inline-item">
                            <a @if(request()->route()->getName() === 'web.delivery') class="active" @endif href="{{route('web.delivery')}}"><span>{{trans('web.menu.delivery')}}</span></a>
                        </li>
                        <li class="list-inline-item">
                            <a @if(request()->route()->getName() === 'web.blog') class="active" @endif href="{{route('web.blog')}}"><span>{{trans('web.menu.blog')}}</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
            <phone-component :phone-kievstar="{{json_encode(getSetting('phone_kievstar'))}}"
                             :phone-vodafone="{{json_encode(getSetting('phone_vodafone'))}}"
                             :phone-life="{{json_encode(getSetting('phone_life'))}}"
                             :phone-city="{{json_encode(getSetting('phone_city'))}}">
            </phone-component>
        </div>
    </div>
</header>
