<link rel="icon" type="image/svg" href="{{asset('images/web/logo/logo.svg')}}" />
<link rel="stylesheet" href="{{asset("fonts/fontawesome_free_6_4_0_web/css/fontawesome.css")}}">
<link rel="stylesheet" href="{{asset("fonts/fontawesome_free_6_4_0_web/css/all.css")}}">
<link rel="stylesheet" href="{{asset("fonts/fontawesome_free_6_4_0_web/css/brands.css")}}">
<link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}">
<link rel="stylesheet" href="{{asset("css/style.css")}}?v=4">
