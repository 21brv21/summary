<div class="bg-footer"></div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-5 col-lg-4">
                <div class="row">
                    <div class="col-5 col-sm-3">
                        <img src="{{asset('images/web/footer_logo.svg')}}">
                    </div>
                    <div class="col-7 col-sm-9">
                        <p>{{trans('web.footer.company')}}</p>
                        <p>{{trans('web.footer.address')}}</p>
                        <p class="small">{{trans('web.footer.water')}}</p>
                    </div>
                    <div class="col-12">
                        <p class="copyright">{{trans('web.footer.tagline')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-2 footer-menu">
                <ul class="list-unstyled">
                    <li><a href="{{route('web.delivery')}}">{{trans('web.menu.delivery')}}</a></li>
                    <li><a href="{{route('web.payment')}}">{{trans('web.menu.payment')}}</a></li>
                    <li><a href="{{route('web.children')}}">{{trans('web.footer.children')}}</a></li>
                    <li><a href="{{route('web.blog')}}">{{trans('web.menu.blog')}}</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3">
                <div class="social d-flex align-items-end">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item">
                            <a href="{{getSetting('facebook')}}" rel="nofollow" target="_blank">
                                <i class="fab fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{getSetting('telegram')}}" rel="nofollow" target="_blank">
                                <i class="fab fa-telegram"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{getSetting('viber')}}" rel="nofollow" target="_blank">
                                <i class="fab fa-viber"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-xl-2 col-lg-3 phones">
                <p><a href="tel:{{getSetting('phone_kievstar')}}">{{getSetting('phone_kievstar')}}</a></p>
                <p><a href="tel:{{getSetting('phone_vodafone')}}">{{getSetting('phone_vodafone')}}</a></p>
                <p><a href="tel:{{getSetting('phone_life')}}">{{getSetting('phone_life')}}</a></p>
                <p><a href="tel:{{getSetting('phone_city')}}">{{getSetting('phone_city')}}</a></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <a class="powered" href="https://horizon.in.ua/" target="_blank" rel="nofollow">
                    @include('web.svg.horizon') Powered by <span
                        style="margin-left: 3px;">Horizon</span>
                </a>
            </div>
        </div>
    </div>
</footer>
