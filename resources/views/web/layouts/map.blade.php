<div class="range">
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-md-5">
                <div class="phone-block">
                    <p><a href="tel:{{getSetting('phone_kievstar')}}">{{getSetting('phone_kievstar')}}</a></p>
                    <p><a href="tel:{{getSetting('phone_vodafone')}}">{{getSetting('phone_vodafone')}}</a></p>
                    <p><a href="tel:{{getSetting('phone_life')}}">{{getSetting('phone_life')}}</a></p>
                    <p><a href="tel:{{getSetting('phone_city')}}">{{getSetting('phone_city')}}</a></p>
                </div>
                <div class="social">
                    <a href="{{getSetting('facebook')}}" rel="nofollow" target="_blank">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a href="{{getSetting('telegram')}}" rel="nofollow" target="_blank">
                        <i class="fab fa-telegram"></i>
                    </a>
                    <a href="{{getSetting('viber')}}" rel="nofollow" target="_blank">
                        <i class="fab fa-viber"></i>
                    </a>
                </div>
                <button onclick="document.getElementById('contactBtn').click();"
                        class="ecommerce ecommerceSelectItemClick ecommerceViewItemClick btn btn-outline-primary btnOrder"
                        id="ecommerceMap"
                        data-ecommerceName="{{trans('web.orderForm.water')}}"
                        data-ecommercePrice="{{getSetting('price_first')}}"
                        data-ecommerceList="Map"
                >
                    {{trans('web.order')}}
                </button>
            </div>
            <div class="col-12 col-md-7">
                <div class="map-block">
                    @include('web.svg.map')
                </div>
            </div>
        </div>
    </div>
</div>
