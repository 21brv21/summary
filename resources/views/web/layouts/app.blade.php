<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('web.layouts.googletagmanager')
    <title>@yield('seo_title', config('app.name'))</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="msapplication-TileColor" content="black">
    <meta name="theme-color" content="black">

    <meta name="description" content="@yield('seo_description', config('app.name'))">
{{--    <meta name="robots" content="noindex,nofollow">--}}

    @yield('seoData')

    <meta property="og:type" content="website">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:title" content="@yield('seo_title', config('app.name'))">
    <meta property="og:image" content="@yield('seo_image' , asset('/images/web/logo/logo.svg'))">
    <meta property="og:description" content="@yield('seo_description', config('app.name'))">
    <meta property="og:site_name" content="{{config('app.name')}}">
    <meta property="og:locale" content="{{app()->getLocale()}}">

    @routes
    @include('web.layouts.style')
</head>
<body>
@include('web.layouts.googletagmanager2')
@include('web.layouts.preloder')
<div id="app">
    @include('web.layouts.header')
    <div class="wrapper page-catalog page-section" id="content">
        @yield('content')
    </div>
    @include('web.layouts.footer')
    @include('web.layouts.modal')
    <div class="displayCenterFlex" id="notification">
        <p></p>
    </div>
    <ecommerce-events :products="{{json_encode(getProducts())}}"
                      :outpost-price="{{json_encode(getSetting('outpost_price'))}}"
                      :price-first="{{json_encode(getSetting('price_first'))}}"
                      :price-second="{{json_encode(getSetting('price_second'))}}"
                      :price-third="{{json_encode(getSetting('price_third'))}}"
                      :sale-price-first="{{json_encode(getSetting('sale_price_first'))}}"
                      :sale-price-second="{{json_encode(getSetting('sale_price_second'))}}"
                      :sale-price-third="{{json_encode(getSetting('sale_price_third'))}}"
    ></ecommerce-events>
</div>

@include('web.layouts.scripts')
@stack('scripts')

</body>
</html>
