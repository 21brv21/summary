<ul>
    <li class="active"><a href="{{route('web.home')}}">{{trans('web.home')}}</a></li>
    <li class="dropdown">
        <a href="{{route('web.catalog')}}">{{trans('web.catalog')}} <span class="icon-chevron-down mobile tablet m-l-15 rotate0"></span></a>
        <ul class="headerMenuDropdown">
            @foreach($globalCategories as $category)
            <li><a href="{{route('web.slug', ['slug' => $category->slug])}}">{{$category->name}}</a></li>
            @endforeach
        </ul>
    </li>
    <li><a href="{{route('web.delivery')}}">{{trans('web.deliveryAndPayment')}}</a></li>
    <li><a href="{{route('web.contacts')}}">{{trans('web.contacts')}}</a></li>
</ul>
