<modal-order-form :products="{{json_encode(getProducts())}}"
                  :outpost-price="{{json_encode(getSetting('outpost_price'))}}"
                  :price-first="{{json_encode(getSetting('price_first'))}}"
                  :price-second="{{json_encode(getSetting('price_second'))}}"
                  :price-third="{{json_encode(getSetting('price_third'))}}"
                  :sale-price-first="{{json_encode(getSetting('sale_price_first'))}}"
                  :sale-price-second="{{json_encode(getSetting('sale_price_second'))}}"
                  :sale-price-third="{{json_encode(getSetting('sale_price_third'))}}"
></modal-order-form>

<div class="modal fade" id="navbarMobile" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="mobile-menu-body">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fas fa-xmark"></i>
                    </button>

                    <div class="mobile-logo"><a href="#"><img src="{{asset('/images/web/logo.png')}}"></a></div>
                    <ul class="nav-wrap text-uppercase list-unstyled">

                        <li class="">
                            <a @if(request()->route()->getName() === 'web.home') class="active" @endif href="{{route('web.home')}}"><span>{{trans('web.menu.home')}}</span></a>
                        </li>
                        <li class="">
                            <a @if(request()->route()->getName() === 'web.payment') class="active" @endif href="{{route('web.payment')}}"><span>{{trans('web.menu.payment')}}</span></a>
                        </li>
                        <li class="">
                            <a @if(request()->route()->getName() === 'web.delivery') class="active" @endif href="{{route('web.delivery')}}"><span>{{trans('web.menu.delivery')}}</span></a>
                        </li>
                        <li class="">
                            <a @if(request()->route()->getName() === 'web.blog') class="active" @endif href="{{route('web.blog')}}"><span>{{trans('web.menu.blog')}}</span></a>
                        </li>
                    </ul>
                    <ul class="social-mobile-menu list-unstyled list-inline">
                        <li class="list-inline-item">
                            <a href="{{getSetting('viber')}}"><i class="fab fa-viber"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{getSetting('telegram')}}"><i class="fas fa-paper-plane"></i></a>
                        </li>
                    </ul>
                    <ul class="lang-mobile-menu list-unstyled list-inline text-uppercase">
                        <li class="list-inline-item">
                            <a @if(app()->getLocale() === 'uk') class="active" @endif href="{{route('web.translate', ['lang' => 'uk'])}}">UK</a>
                        </li>
                        <li class="list-inline-item">
                            <a @if(app()->getLocale() === 'en') class="active" @endif href="{{route('web.translate', ['lang' => 'en'])}}">EN</a>
                        </li>
                        <li class="list-inline-item">
                            <a @if(app()->getLocale() === 'ru') class="active" @endif href="{{route('web.translate', ['lang' => 'ru'])}}">RU</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
