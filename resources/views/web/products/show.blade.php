@extends('web.layouts.app')

@section('seo_title', $product->translation->seo_title)
@section('seo_description', $product->translation->seo_description)

@section('seoData')
    @foreach($seoSchemas as $key => $seoSchema)<script type="application/ld+json" data-seo="{{$key}}">{!! $seoSchema !!}</script>@endforeach
@endsection

@section('content')
    <section class="productSection m-t-50 m-b-30 m-b-15">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="text-center page-caption">{{trans('web.catalog')}}</p>
                </div>
                <div class="row catalog-item">
                    <div class="col-12 col-md-5">
                        <div class="thumb">
                            <div class="image text-center">
                                <a href="#">
                                    <img src="{{$product->image}}" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="article-content">
                            <p class="text-uppercase name">{{$product->name}}</p>
                            <p>{{$product->translation->short_description}}</p>
                            <p class="price-block">
                                <span class="price">{{intval($product->price)}} {{trans('web.grn')}}</span>
                            </p>
                            <button onclick="document.getElementById('contactBtn').click();"
                                    class="btn btn-primary ecommerce ecommerceSelectItemClick ecommerceViewItemClick"
                                    id="ecommerceCatalog"
                                    data-ecommerceName="{{$product->name}}"
                                    data-ecommercePrice="{{intval($product->price)}}"
                                    data-ecommerceList="Catalog"
                            >
                                {{trans('web.buy')}}
                            </button>
                        </div>
                    </div>
                    @if($product->translation->options && count($product->translation->options) > 0)
                        @foreach($product->translation->options as $name => $value)
                            <div class="col-12 m-b-15">
                                <p class="text-center h2">{{trans('web.characteristics')}}</p>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>{{$name}}:</td>
                                        <td>{{$value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endforeach
                    @endif
                    <div class="col-12 description">
                        {!! $product->translation->description !!}
                    </div>

                    @if(count($product->reviews) > 0)
                        <div class="col-12">
                            <p class="text-center h2">{{trans('web.reviews')}}</p>
                            <div class="review">
                                <p class="name">Антон Усачов</p>
                                <p>Втомилися від несмачної води та незручних фільтрів? Шукаєте, де замовити воду в Києві за вигідною ціною з хорошим сервісом? Тоді ласкаво просимо - вас вітає найкраща служба доставки води в столиці. Ми обслуговуємо всі райони Києва, винищуючи спрагу на корені.</p>
                            </div>
                            <div class="review">
                                <div class="review-btn text-center">
                                    <a href="#" class="btn btn-outline-primary" class="btn btn-primary">
                                    {{trans('web.addReview')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('web.layouts.map')
@endsection
