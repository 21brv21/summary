export default {
    methods: {
        unFormatPhone(phone) {
            if (phone) {
                phone = phone.replace(/-/g, '');
                phone = phone.replace('(', '');
                phone = phone.replace(')', '');
                phone = phone.replace('+', '');
            }
            return phone;
        },
        formatPhone(phone) {
            var x = phone.replace(/\D/g, '').match(/(\d{0,2})(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
            return x[1] + "(" + x[2] + ')' + x[3] + '-' + x[4] + '-' + x[5]
        },
        setFormatPhones(users) {
            users.forEach(user =>{
                user.phone = this.formatPhone(user.phone)
            });
            return users;
        },
    }
}
