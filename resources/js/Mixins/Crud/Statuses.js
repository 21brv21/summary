export default {
    methods: {

        setNewStatus(event, id) {
            let tr = this.getTrElementFromEvent(event);
            this.LoaderIndicator(true)
            axios.post("/admin/api/v1/orders/set-new-status/" + id)
                .then(response => {
                    tr.setAttribute("style", "display: none");
                    this.LoaderIndicator(false);
                }).catch(error => {
                    this.LoaderIndicator(false);
                }
            );
        },

        setPaymentStatus(event, id) {
            let tr = this.getTrElementFromEvent(event);
            this.LoaderIndicator(true)
            axios.post("/admin/api/v1/orders/set-payment-status/" + id)
                .then(response => {
                    tr.setAttribute("style", "display: none");
                    this.LoaderIndicator(false);
                }).catch(error => {
                    this.LoaderIndicator(false);
                }
            );
        },

        setCompletedStatus(event, id) {
            let tr = this.getTrElementFromEvent(event);
            this.LoaderIndicator(true)
            axios.post("/admin/api/v1/orders/set-completed-status/" + id)
                .then(response => {
                    tr.setAttribute("style", "display: none");
                    this.LoaderIndicator(false);
                }).catch(error => {
                    this.LoaderIndicator(false);
                }
            );
        },

        setCanceledStatus(event, id) {
            let tr = this.getTrElementFromEvent(event);
            this.LoaderIndicator(true)
            axios.post("/admin/api/v1/orders/set-canceled-status/" + id)
                .then(response => {
                    tr.setAttribute("style", "display: none");
                    this.LoaderIndicator(false);
                }).catch(error => {
                    this.LoaderIndicator(false);
                }
            );
        },

        getTrElementFromEvent(event){
            return event.target.parentElement.parentElement.parentElement.parentElement;
        }
    }
}
