//DEFAULT
import TextColumn from '../../Components/Crud/Columns/TextColumn'
import BooleanColumn from '../../Components/Crud/Columns/BooleanColumn'
import ColorColumn from '../../Components/Crud/Columns/ColorColumn'
import MultipleColumn from '../../Components/Crud/Columns/MultipleColumn'
import PhoneColumn from '../../Components/Crud/Columns/PhoneColumn'
import NumberColumn from '../../Components/Crud/Columns/NumberColumn'
import ImageColumn from '../../Components/Crud/Columns/ImageColumn'


//CUSTOM
import GrnColumn from '../../Components/Crud/Columns/GrnColumn'
import EuroColumn from '../../Components/Crud/Columns/EuroColumn'
import SetStatusColumn from '../../Components/Crud/Columns/SetStatusColumn'


export default {
    components: {
        TextColumn,
        BooleanColumn,
        ColorColumn,
        MultipleColumn,
        GrnColumn,
        EuroColumn,
        PhoneColumn,
        SetStatusColumn,
        NumberColumn,
        ImageColumn,
    },
}
