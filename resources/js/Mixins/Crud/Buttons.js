import LinkButton from '../../Components/Crud/Buttons/LinkButton'
import ClickButton from '../../Components/Crud/Buttons/ClickButton'


export default {
    components: {
        LinkButton,
        ClickButton,
    },
}
