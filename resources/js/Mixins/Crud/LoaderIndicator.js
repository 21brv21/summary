export default {
    methods: {
        LoaderIndicator(bool) {
            var preloder = document.getElementById('preloder');
            var loader = document.getElementsByClassName('loader')[0];
            if (bool) {
                loader.style.display = 'block';
                preloder.style.display = 'block';
                preloder.style.background = 'transparent';
            } else {
                loader.style.display = 'none';
                preloder.style.display = 'none';
            }
        },
    }
}
