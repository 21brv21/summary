import TextInput from '../../Components/Crud/Fields/TextInput'
import PhoneInput from '../../Components/Crud/Fields/PhoneInput'
import EditorInput from '../../Components/Crud/Fields/EditorInput'
import ImageInput from '../../Components/Crud/Fields/ImageInput'
import JsonInput from '../../Components/Crud/Fields/JsonInput'
import Select2MultipleInput from '../../Components/Crud/Fields/Select2MultipleInput'
import SlugInput from '../../Components/Crud/Fields/SlugInput'
import Select2ArrayInput from '../../Components/Crud/Fields/Select2ArrayInput'
import Select2Input from '../../Components/Crud/Fields/Select2Input'
import DateInput from '../../Components/Crud/Fields/DateInput'
import ColorInput from '../../Components/Crud/Fields/ColorInput'
import BooleanInput from '../../Components/Crud/Fields/BooleanInput'
import RadioInput from '../../Components/Crud/Fields/RadioInput'
import RadioColorInput from '../../Components/Crud/Fields/RadioColorInput'
import TextareaInput from '../../Components/Crud/Fields/TextareaInput'
import NumberInput from '../../Components/Crud/Fields/NumberInput'
import FileInput from '../../Components/Crud/Fields/FileInput'
import HasManyInput from '../../Components/Crud/Fields/HasManyInput'
import HasManyTextInput from '../../Components/Crud/Fields/HasManyTextInput'
import SelectInput from '../../Components/Crud/Fields/SelectInput'
import EdrpouInput from '../../Components/Crud/Fields/EdrpouInput'
import SeoTextInput from '../../Components/Crud/Fields/SeoTextInput'
import SeoTextareaInput from '../../Components/Crud/Fields/SeoTextareaInput'
import FloatInput from '../../Components/Crud/Fields/FloatInput'

export default {
    components: {
        TextInput,
        TextareaInput,
        PhoneInput,
        EditorInput,
        ImageInput,
        JsonInput,
        Select2MultipleInput,
        SlugInput,
        Select2Input,
        Select2ArrayInput,
        ColorInput,
        DateInput,
        BooleanInput,
        RadioInput,
        RadioColorInput,
        NumberInput,
        FileInput,
        HasManyInput,
        HasManyTextInput,
        SelectInput,
        EdrpouInput,
        SeoTextInput,
        SeoTextareaInput,
        FloatInput,
    },
}
