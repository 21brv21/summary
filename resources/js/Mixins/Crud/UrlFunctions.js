export default {
    methods: {
        insertParam(key, value) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);

            let kvp = window.location.search.substr(1).split('&');
            if (kvp[0] === '') {
                const path = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + key + '=' + value;
                window.history.pushState({path: path}, '', path);

            } else {
                let i = kvp.length;
                let x;
                while (i--) {
                    x = kvp[i].split('=');

                    if (x[0] === key) {
                        x[1] = value;
                        kvp[i] = x.join('=');
                        break;
                    }
                }

                if (i < 0) {
                    kvp[kvp.length] = [key, value].join('=');
                }

                const refresh = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + kvp.join('&');
                window.history.pushState({path: refresh}, '', refresh);
            }
        },
        deleteParam(parameter) {
            let url = window.location.href;
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {

                var prefix = encodeURIComponent(parameter) + '=';
                var pars = urlparts[1].split(/[&;]/g);
                for (var i = pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                let path = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '')
                return window.history.pushState({path: path}, '', path);
            }
            return url;
        },
        getParam(filterName) {
            var queryString = window.location.search;
            var urlParams = new URLSearchParams(queryString);
            return urlParams.get(filterName) ? urlParams.get(filterName) : null
        },
        getOnlyUrl(){
            return  window.location.href.split('?')[0];
        },
        deleteAllParams() {
            const refresh = window.location.protocol + "//" + window.location.host + window.location.pathname;
            window.history.pushState({path: refresh}, '', refresh);
        },
        getAllParameters() {
            let params = '';
            let queryString = window.location.search;
            let urlParams = new URLSearchParams(queryString);
            let keys =urlParams.entries();
            for (let key of keys) {
                params += '&' + key[0] + '=' + key[1]
            }
            return params;
        },
    }
}
