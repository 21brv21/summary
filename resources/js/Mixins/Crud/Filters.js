import Select2Filter from '../../Components/Crud/Filters/Select2Filter'
import Select2KeyFilter from '../../Components/Crud/Filters/Select2KeyFilter'
import SearchApiFilter from '../../Components/Crud/Filters/SearchApiFilter'


export default {
    components: {
        Select2Filter,
        Select2KeyFilter,
        SearchApiFilter,
    },
    methods: {

    }
}
