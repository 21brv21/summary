import {mask} from 'vue-the-mask'

export default {
    directives: {mask},
    methods: {
        showMessage(message) {
            let el = document.getElementById('notification');
            let text = el.firstChild;
            text.innerHTML = message;
            el.style.maxHeight = text.scrollHeight + 20 + "px";
            setTimeout(() => el.style.maxHeight = null, 6000);
        },
        showErrors(error) {
            let errors = [];
            if (typeof (error.response.data.errors) != "undefined") {
                for (let [key, value] of Object.entries(error.response.data.errors)) {
                    if (Array.isArray(value)) {
                        for (var i = 0; i < value.length; i++) {
                            console.log(value[i])
                            errors.push(value[i])
                        }
                    } else {
                        errors.push(value)
                    }
                }
            } else if(error.response.data.error) {
                errors.push([error.response.data.error])
            } else {
                errors.push([error.response.data.message])
            }
            this.showMessage(errors.join('<br>'))
        },
    }
}
