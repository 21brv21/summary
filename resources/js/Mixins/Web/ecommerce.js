export default {
    props: {
        item_name: String,
        item_list_name: String,
        price: Number,
    },
    data: () => ({
        item_id: '',
        item_brand: '',
        item_category: '',
        viewedId: [],
        pumpId: 1,
        coolerId: 2,
        dispenserId: 3,
    }),
    methods: {
        handleScroll() {
            // Отримати висоту вікна браузера
            const windowHeight = window.innerHeight;

            // Пройтися по всім елементам товарів і перевірити, чи вони видимі
            const els = document.getElementsByClassName('ecommerce');

            for (let i = 0; i < els.length; i++) {
                let el = els[i];
                if (el.getBoundingClientRect().top < windowHeight && !this.viewedId.includes(el.id)) {
                    this.viewedId.push(el.id);
                    let name = el.getAttribute('data-ecommerceName');
                    let price = el.getAttribute('data-ecommercePrice');
                    let list_name = el.getAttribute('data-ecommerceList');
                    this.ecommerceSendEvent(name, price, list_name, 'view_item_list');
                }
            }
        },
        ecommerceSelectItemEvent(event) {
            let el = event.target;
            let name = el.getAttribute('data-ecommerceName');
            let price = el.getAttribute('data-ecommercePrice');
            let list_name = el.getAttribute('data-ecommerceList');
            this.ecommerceSendEvent(name, price, list_name, 'select_item');
        },
        ecommerceSendEvent(name, price, list_name, eventName) {
            let items = [{
                item_name: name,
                item_id: this.item_id,
                price: this.convertToPrice(price),
                item_brand: this.item_brand,
                item_category: this.item_category,
                item_list_name: list_name
            }];
            this.ecommerceSendItemEvent(items, eventName);
        },
        ecommerceSendCartEvent(name, price, quantity, isAddToCart) {
            let eventName = isAddToCart ? 'add_to_cart' : 'remove_from_cart';
            let items = [{
                item_name: name,
                item_id: this.item_id,
                price: this.convertToPrice(price),
                item_brand: this.item_brand,
                item_category: this.item_category,
                item_list_name: 'Form',
                quantity: quantity
            }];
            this.ecommerceSendItemEvent(items, eventName);
        },
        ecommerceSendCartInfoEvent(name, price, eventName) {
            let items = [{
                item_name: name,
                item_id: this.item_id,
                price: this.convertToPrice(price),
                item_brand: this.item_brand,
                item_category: this.item_category,
                quantity: 1,
                item_list_name: 'Form',
            }];
            this.ecommerceSendItemEvent(items, eventName);
        },
        ecommerceSendItemEvent(items, eventName) {
            window.dataLayer.push({ecommerce: null});
            window.dataLayer.push({
                event: eventName,
                ecommerce: {
                    currency: "UAH",
                    value: this.getTotalPriceFromItems(items),
                    items: items
                }
            });
        },
        ecommerceSendOrderEvent(order) {
            let items = [];
            if (order.bottle_quantity > 0){
                items.push({
                    item_name: this.trans('web.water'),
                    price: this.convertToPrice(order.bottle_price),
                    quantity: order.bottle_quantity,
                    item_id: this.item_id,
                    item_brand: this.item_brand,
                    item_category: this.item_category,
                });
            }

            if (order.outpost_quantity > 0){
                items.push({
                    item_name: 'Deposit',
                    price: this.convertToPrice(order.outpost_price),
                    quantity: order.outpost_quantity,
                    item_id: this.item_id,
                    item_brand: this.item_brand,
                    item_category: this.item_category,
                });
            }

            for (let i = 0; i < order.products.length; i++) {
                let product = order.products[i];
                items.push({
                    item_name: product.translation.name,
                    price: this.convertToPrice(product.pivot.price),
                    quantity: product.pivot.quantity,
                    item_id: this.item_id,
                    item_brand: this.item_brand,
                    item_category: this.item_category,
                });
            }

            dataLayer.push({ecommerce: null});  // Clear the previous ecommerce object.
            dataLayer.push({
                event: "purchase",
                ecommerce: {
                    transaction_id: order.id.toString(),  // Передаємо ідентифікатор замовлення (з адмінки, лише цифри)
                    value: this.convertToPrice(order.total_price),  // Передаємо загальну суму
                    affiliation: order.is_new_user ? 'New' : 'Old',
                    currency: "UAH",  // Передаємо валюту
                    tax: 0,
                    shipping: 0,
                    items: items

                }
            });
        },
        ecommerceCreateItem(name, price, list_name) {
            return {
                item_name: name,
                price: this.convertToPrice(price),
                item_id: this.item_id,
                item_brand: this.item_brand,
                item_category: this.item_category,
                item_list_name: list_name
            };
        },
        ecommerceSendViewItemEvent() {
            let items = [];
            items.push(this.ecommerceCreateItem(this.trans('web.water'), this.priceFirst, 'Form'));
            items.push(this.ecommerceCreateItem(this.trans('web.orderForm.outpost'), this.outpostPrice, 'Form'));
            items.push(this.ecommerceCreateItem(this.getProductNameById(this.products, this.pumpId), this.getProductPriceById(this.products, this.pumpId), 'Form'));
            items.push(this.ecommerceCreateItem(this.getProductNameById(this.products, this.coolerId), this.getProductPriceById(this.products, this.coolerId), 'Form'));
            items.push(this.ecommerceCreateItem(this.getProductNameById(this.products, this.dispenserId), this.getProductPriceById(this.products, this.dispenserId), 'Form'));
            this.ecommerceSendItemEvent(items, 'view_item');
        },
        getCurrentTime() {
            const now = new Date();
            let hours = now.getHours();
            let minutes = now.getMinutes();

            // Округлення до ближнього числа, кратного 30
            minutes = Math.round(minutes / 30) * 30;

            // Форматування годин і хвилин
            if (minutes === 60) {
                hours++;
                minutes = 0;
            }
            return `${hours}:${minutes.toString().padStart(2, '0')}`;
        },
        convertToPrice(price){
            return parseFloat(price);
        },
        getTotalPriceFromItems(items){
            let total = 0;
            items.forEach(item => {
                total += item.price;
            });
            return total;
        },
        getCurrentDayOfWeek() {
            let now = new Date();
            let daysOfWeek = ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота'];
            return daysOfWeek[now.getDay()];
        },
        getCurrentMonth() {
            let now = new Date();
            return now.getMonth() + 1;
        },
        getProductPriceById(products, id) {
            let price = 0;
            for (let i = 0; i < products.length; i++) {
                let product = products[i];
                if (product.id === id) {
                    price = product.price;
                }
            }
            return price;
        },
        getProductNameById(products, id) {
            let price = 0;
            for (let i = 0; i < products.length; i++) {
                let product = products[i];
                if (product.id === id) {
                    price = product.translation.name;
                }
            }
            return price;
        }
    },
    created() {
        this.item_id = this.getCurrentMonth();
        this.item_brand = this.getCurrentDayOfWeek();
        this.item_category = this.getCurrentTime();
    },
}
