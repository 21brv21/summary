export default {
    props: {
        id: String,
        products: Array,
        priceFirst: Number,
        priceSecond: Number,
        priceThird: Number,
        salePriceFirst: Number,
        salePriceSecond: Number,
        salePriceThird: Number,
        outpostPrice: Number,
    },
    data: () => ({
        disabledBtn: false,
        isNewUser: false,
        src: '/images/web/water/butl-3.webp',
        srcButl: '/images/web/water/butl.webp',
        name: '',
        phone: '',
        address: '',
        productsId: {},
        productsBuy: [],
        priceShow: 0,
        bottleQuantity: 0,
        outpostQuantity: 0,
        totalPrice: 0,
        componentKey: 0,
        componentKeyQuantity: 0,

        isSendMode: true,
        payment:{
            rr: '',
            ipn: '',
            mfo: '',
            bank: '',
            price: '',
            legalName: '',
            total: 0,
        }
    }),
    methods: {
        calculateTotalPrice() {
            this.totalPrice = 0;
            this.totalPrice += this.calculateTotalPriceProduct();
            this.totalPrice += this.outpostPrice * this.outpostQuantity;

            if (this.isNewUser) {
                if (this.bottleQuantity < 3) {
                    this.totalPrice += this.salePriceFirst * this.bottleQuantity;
                    this.priceShow = this.salePriceFirst;
                } else if (this.bottleQuantity >= 3 && this.bottleQuantity < 10) {
                    this.totalPrice += this.salePriceSecond * this.bottleQuantity - this.salePriceSecond;
                    this.priceShow = this.salePriceSecond;
                } else {
                    this.totalPrice += this.salePriceThird * this.bottleQuantity;
                    this.priceShow = this.salePriceThird;
                }
            } else {
                if (this.bottleQuantity < 3) {
                    this.totalPrice += this.priceFirst * this.bottleQuantity;
                    this.priceShow = this.priceFirst;
                } else if (this.bottleQuantity >= 3 && this.bottleQuantity < 8) {
                    this.totalPrice += this.priceSecond * this.bottleQuantity;
                    this.priceShow = this.priceSecond;
                } else {
                    this.totalPrice += this.priceThird * this.bottleQuantity;
                    this.priceShow = this.priceThird;
                }
            }

            this.totalPrice = Math.round(this.totalPrice);
        },
        calculateTotalPriceProduct() {
            this.productsBuy = [];
            let total = 0;
            for (let i = 0; i < this.products.length; i++) {
                if (this.productsId[this.products[i].id]) {
                    let item = {
                        id: this.products[i].id,
                        price: this.products[i].price,
                        quantity: 1,
                    };
                    this.productsBuy.push(item);
                    total += Number(this.products[i].price);
                }
            }
            return total;
        },
        checkBottle(bottleQuantity) {
            if (bottleQuantity === 1) {
                this.bottleQuantity = 2;
            }
            this.calculateTotalPrice();
            this.componentKeyQuantity++;
        },
        reset() {
            for (let i = 0; i < this.products.length; i++) {
                this.productsId[this.products[i].id] = false;
            }
            this.productsBuy = [];
            this.name = '';
            this.phone = '';
            this.address = '';
            this.bottleQuantity = 0;
            this.outpostQuantity = 0;
            this.componentKey++;
            this.disabledBtn = false;
        },
    }
}
