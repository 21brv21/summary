require('./bootstrap');

import {createApp, h} from 'vue';
import {createInertiaApp} from '@inertiajs/inertia-vue3';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Vodoliy';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => require(`./Pages/${name}.vue`),
    setup({el, app, props, plugin}) {
        return createApp({render: () => h(app, props)})
            .use(plugin)
            .mixin({
                methods: {
                    route,
                    LoaderIndicator(bool) {
                        let preloder = document.getElementById('preloder');
                        preloder.style.display = bool ? 'flex' : 'none';
                    },
                    trans(key, replace = {}) {
                        let keys = key.split('.');
                        let translation = this.$page.props.language;
                        keys.forEach(function(keyTmp){
                            translation = translation[keyTmp]
                                ? translation[keyTmp]
                                : keyTmp
                        });

                        Object.keys(replace).forEach(function (key) {
                            translation = translation.replace(':' + key, replace[key])
                        });

                        return translation
                    },
                }
            })
            .mount(el);
    },
});
