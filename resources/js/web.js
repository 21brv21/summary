require('./bootstrap')

import {createApp} from 'vue'

const app = createApp({
    data: () => ({
        locale: null,
        translation: null,
    }),
    created() {
        this.locale = document.documentElement.lang;
        this.translation = require('../../lang/' + this.locale + '.json');
    }
})

import ModalContactForm from './Components/Web/Modals/ModalContactForm';
import ModalOrderForm from './Components/Web/Modals/ModalOrderForm';
import ModalReviewComponent from './Components/Web/Modals/ModalReviewComponent';
import OrderForm from './Components/Web/Order/OrderForm';
import PhoneComponent from './Components/Web/Layouts/PhoneComponent';

import EcommerceEvents from './Components/Web/Ecommerce/EcommerceEvents';

app.component('modal-contact-form', ModalContactForm);
app.component('modal-order-form', ModalOrderForm);
app.component('modal-review-form', ModalReviewComponent);
app.component('order-form', OrderForm);
app.component('phone-component', PhoneComponent);

app.component('ecommerce-events', EcommerceEvents);

app.mixin({
    methods: {
        route,
        LoaderIndicator(bool) {
            document.getElementById('preloder').style.display = bool ? 'flex' : 'none';
        },

        trans(key, replace = {}) {
            let keys = key.split('.');
            var translation = this.$root.translation;
            keys.forEach(function (keyTmp) {
                translation = translation[keyTmp]
                    ? translation[keyTmp]
                    : keyTmp
            });

            Object.keys(replace).forEach(function (key) {
                translation = translation.replace(':' + key, replace[key])
            });
            return translation;
        },
    }
});
app.mount('#app');
