<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\Pages\PagesController;

Route::get('water',  [PagesController::class, 'water'])->name('web.water');
Route::get('blog',  [PagesController::class, 'blog'])->name('web.blog');
Route::get('delivery',  [PagesController::class, 'delivery'])->name('web.delivery');
Route::get('payment',  [PagesController::class, 'payment'])->name('web.payment');
Route::get('children',  [PagesController::class, 'children'])->name('web.children');
Route::get('google-sitemap',  [PagesController::class, 'sitemap'])->name('web.sitemap');
