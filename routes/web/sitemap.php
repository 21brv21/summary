<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\Pages\SitemapController;

Route::get('sitemap-products',  [SitemapController::class, 'sitemapProducts'])->name('web.sitemap.products');
Route::get('sitemap-categories',  [SitemapController::class, 'sitemapCategories'])->name('web.sitemap.categories');
Route::get('sitemap-pages',  [SitemapController::class, 'sitemapPages'])->name('web.sitemap.pages');
Route::get('sitemap-blog',  [SitemapController::class, 'sitemapBlog'])->name('web.sitemap.blog');
