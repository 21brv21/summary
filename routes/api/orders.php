<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Orders\ApiContactFormController;
use App\Http\Controllers\Api\Orders\ApiOrderController;

Route::post('contact-form', [ApiContactFormController::class, 'send']);
Route::post('create', [ApiOrderController::class, 'store']);
