<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\Pages\PagesController;

Route::get('/',  [PagesController::class, 'home'])->name('web.home');
Route::get('translate/{lang}',  [PagesController::class, 'translate'])->name('web.translate');
Route::get('public/{slug}',  [PagesController::class, 'public'])->name('web.public')->where('slug', '.*');
Route::get('sitemap.xml',  [PagesController::class, 'sitemap'])->name('web.sitemap');
Route::get('/{slug}',  [PagesController::class, 'slug'])->name('web.slug')->where('slug', '.*');
