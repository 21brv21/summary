<?php

use App\Services\Crud\CrudRoute;
use App\Http\Controllers\Admin\Users\AdminsCrudController;
use App\Http\Controllers\Admin\Users\ClientsCrudController;

CrudRoute::resource('admins', AdminsCrudController::class);
CrudRoute::resource('clients', ClientsCrudController::class);
