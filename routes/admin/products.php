<?php

use App\Services\Crud\CrudRoute;
use App\Http\Controllers\Admin\Products\ProductsCrudController;
use App\Http\Controllers\Admin\Products\ReviewsCrudController;

CrudRoute::resource('reviews', ReviewsCrudController::class);
CrudRoute::resource('products', ProductsCrudController::class);

