<?php

use Illuminate\Support\Facades\Route;
use App\Services\Crud\CrudRoute;
use App\Http\Controllers\Admin\Pages\PagesController;
use App\Http\Controllers\Admin\Pages\PagesCrudController;
use App\Http\Controllers\Admin\Pages\BlogCrudController;

Route::get('dashboard',  [PagesController::class, 'dashboard'])->name('dashboard');
CrudRoute::resource('ref-pages', PagesCrudController::class);
CrudRoute::resource('blog', BlogCrudController::class);
