<?php

use App\Services\Crud\CrudRoute;
use App\Http\Controllers\Admin\Orders\ContactFormsCrudController;
use App\Http\Controllers\Admin\Orders\OrdersCrudController;

CrudRoute::resource('contact-forms', ContactFormsCrudController::class);
CrudRoute::resource('orders', OrdersCrudController::class);
