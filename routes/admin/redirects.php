<?php

use App\Services\Crud\CrudRoute;
use App\Http\Controllers\Admin\Redirects\RefRedirectsCrudController;

CrudRoute::resource('redirects', RefRedirectsCrudController::class);
