<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Ref\RefSettingCrudController;
use App\Http\Controllers\Admin\Ref\RefLanguagesCrudController;
use App\Http\Controllers\Admin\Ref\RefPagesCrudController;
use App\Services\Crud\CrudRoute;
use App\Http\Controllers\Admin\Ref\RefSettingFileController;

CrudRoute::resource('ref-settings', RefSettingCrudController::class);
CrudRoute::resource('ref-languages', RefLanguagesCrudController::class);
CrudRoute::resource('ref-pages', RefPagesCrudController::class);

Route::get('ref-certificate',  [RefSettingFileController::class, 'get'])->name('admin.ref-certificate');
Route::post('ref-certificate',  [RefSettingFileController::class, 'update']);

