<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Files\ImageController;

Route::post('{model}/images/{modelId}',  [ImageController::class, 'store']);
Route::delete('{model}/images/{image}',  [ImageController::class, 'destory']);
