<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Значення :attribute повинні бути прийняті.',
    'active_url' => 'Значення :attribute is not a valid URL.',
    'after' => 'Значення :attribute must be a date after :date.',
    'after_or_equal' => 'Значення :attribute must be a date after or equal to :date.',
    'alpha' => 'Значення :attribute must only contain letters.',
    'alpha_dash' => 'Значення :attribute must only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'Значення must only contain letters and numbers.',
    'array' => 'Значення must be an array.',
    'before' => 'Значення must be a date before :date.',
    'before_or_equal' => 'Значення must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'Значення must be between :min and :max.',
        'file' => 'Значення must be between :min and :max kilobytes.',
        'string' => 'Значення must be between :min and :max characters.',
        'array' => 'Значення must have between :min and :max items.',
    ],
    'boolean' => 'Значення має бути true або false!',
    'confirmed' => 'Значення confirmation does not match.',
    'current_password' => 'The password is incorrect.',
    'date' => 'Значення не є датою.',
    'date_equals' => 'Значення must be a date equal to :date.',
    'date_format' => 'Значення does not match the format :format.',
    'different' => 'Значення and :other must be different.',
    'digits' => 'Значення must be :digits digits.',
    'digits_between' => 'Значення must be between :min and :max digits.',
    'dimensions' => 'Значення has invalid image dimensions.',
    'distinct' => 'Значення field has a duplicate value.',
    'email' => 'Значення must be a valid email address.',
    'ends_with' => 'Значення must end with one of the following: :values.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'Значення must be a file.',
    'filled' => 'Значення field must have a value.',
    'gt' => [
        'numeric' => 'Значення must be greater than :value.',
        'file' => 'Значення must be greater than :value kilobytes.',
        'string' => 'Значення must be greater than :value characters.',
        'array' => 'Значення must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'Значення must be greater than or equal :value.',
        'file' => 'Значення must be greater than or equal :value kilobytes.',
        'string' => 'Значення must be greater than or equal :value characters.',
        'array' => 'Значення must have :value items or more.',
    ],
    'image' => 'Значення must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'Значення field does not exist in :other.',
    'integer' => 'Значення must be an integer.',
    'ip' => 'Значення must be a valid IP address.',
    'ipv4' => 'Значення must be a valid IPv4 address.',
    'ipv6' => 'Значення must be a valid IPv6 address.',
    'json' => 'Значення must be a valid JSON string.',
    'lt' => [
        'numeric' => 'Значення must be less than :value.',
        'file' => 'Значення must be less than :value kilobytes.',
        'string' => 'Значення must be less than :value characters.',
        'array' => 'Значення must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'Значення must be less than or equal :value.',
        'file' => 'Значення must be less than or equal :value kilobytes.',
        'string' => 'Значення must be less than or equal :value characters.',
        'array' => 'Значення must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'Значення не має перевищувати :макс.',
        'file' => 'Значення не повинно перевищувати :max кілобайт.',
        'string' => 'Значення не повинно перевищувати :max символів.',
        'array' => 'Значення не повинно мати більше ніж :max елементів.',
    ],
    'mimes' => 'Значення must be a file of type: :values.',
    'mimetypes' => 'Значення must be a file of type: :values.',
    'min' => [
        'numeric' => 'Значення має бути не менше :min.',
        'file' => 'Значення має бути не менше :min кілобайт.',
        'string' => 'Значення має містити принаймні :min символів.',
        'array' => 'Значення повинно мати принаймні :min елементів.',
    ],
    'multiple_of' => 'Значення must be a multiple of :value.',
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'Значення format is invalid.',
    'numeric' => 'Значення має бути числом!',
    'password' => 'The password is incorrect.',
    'present' => 'Значення field must be present.',
    'regex' => 'Значення format is invalid.',
    'required' => 'Значення є обов\'язковим!',
    'required_if' => 'Значення field is required when :other is :value.',
    'required_unless' => 'Значення field is required unless :other is in :values.',
    'required_with' => 'Значення field is required when :values is present.',
    'required_with_all' => 'Значення field is required when :values are present.',
    'required_without' => 'Значення field is required when :values is not present.',
    'required_without_all' => 'Значення field is required when none of :values are present.',
    'prohibited' => 'Значення field is prohibited.',
    'prohibited_if' => 'Значення field is prohibited when :other is :value.',
    'prohibited_unless' => 'Значення field is prohibited unless :other is in :values.',
    'same' => 'Значення and :other must match.',
    'size' => [
        'numeric' => 'Значення must be :size.',
        'file' => 'Значення must be :size kilobytes.',
        'string' => 'Значення must be :size characters.',
        'array' => 'Значення must contain :size items.',
    ],
    'starts_with' => 'Значення must start with one of the following: :values.',
    'string' => 'Значення must be a string.',
    'timezone' => 'Значення must be a valid timezone.',
    'unique' => 'Значення has already been taken.',
    'uploaded' => 'Значення failed to upload.',
    'url' => 'Значення must be a valid URL.',
    'uuid' => 'Значення must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

    'slug' => [
        'required' => 'Необхідно вказати slug',
        'unique' => 'Такий slug вже існує',
    ],
    'price' => [
        'required' => 'Необхідно вказати ціну',
    ],
    'quantity' => [
        'required' => 'Необхідно вказати кількість',
    ],
    'name' => [
        'required' => 'Необхідно вказати назву',
    ],
    'consist' => [
        'required' => 'Необхідно вказати склад',
    ],
    'description' => [
        'required' => 'Необхідно вказати опис',
    ],
    'seo_title' => 'Необхідно вказати seo_title',
    'seo_description' => 'Необхідно вказати seo_description',
    'filters' => 'Необхідно обрати мінімум один фільтр',
    'categories' => 'Необхідно обрати мінімум одну категорію',
    'manufacturer' => 'Необхідно обрати виробника',
    'nameUser' => 'Невірно введено ім\'я',
    'phone' => 'Невірно введено номер телефону',
    'phoneUnique' => 'Такий номер телефону вже використовується',
    'phoneOperator' => 'Такого оператора не існує',
    'bottleQuantity' => 'Невірна кількість бутлів',
];
