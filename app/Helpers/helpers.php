<?php

use App\Models\Products\Product;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


//FUNCTIONS

if (!function_exists('isProd')) {
    function isProd()
    {
        return (config('app.env') == 'production');
    }
}

if (!function_exists('generatePassword')) {
    function generatePassword()
    {
        return rand(100000, 999999);
    }
}

if (!function_exists('translations')) {
    function translations($json)
    {
        if (!file_exists($json)) {
            return [];
        }
        return json_decode(file_get_contents($json), true);
    }
}

if (!function_exists('getLanguages')) {
    function getLanguages()
    {
        if (!Cache::has(LANGUAGE_ALL_CACHE)) {
            $languages = DB::table('ref_languages')
                ->select('*')
                ->get();
            Cache::forever(LANGUAGE_ALL_CACHE, $languages);
        }
        return Cache::get(LANGUAGE_ALL_CACHE);
    }
}


if (!function_exists('formattedPhone')) {
    function formattedPhone($phone): string
    {
        return str_replace(array("+", "(", ")", "-"), "", $phone);
    }
}

if (!function_exists('getSetting')) {
    function getSetting(string $value): string
    {
        return App\Models\Ref\RefSetting::getSettingByKey($value);
    }
}


if (!function_exists('getProducts')) {
    function getProducts()
    {
        return App\Models\Products\Product::with('translation')->get();
    }
}
