<?php

const LANGUAGE_ALL_CACHE = 'language_all';
const LANGUAGE_WITHOUT_ACTIVE_CACHE = 'language_without_active_';

const PAGE_ALL_CACHE = 'page_all_';
const PAGE_BY_ID_CACHE = '_page_by_id_';
const PRODUCT_ALL_CACHE = 'product_all_';
const PRODUCT_NAME_CACHE = '_product_name_';

const SETTING_BY_KET_CACHE = '_setting_by_key';
const SLUG_MODEL_CACHE = '_slug_model';

const BLOG_LATEST_CACHE = '_blog_latest_';
const BLOG_BY_ID_CACHE = '_blog_by_id_';
