<?php


namespace App\Traits\Models;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait HasImageTrait
{
    public function getImageAttribute($value): ?string
    {
        return !is_null($value) ? Storage::disk('public')->url($value) : null;
    }

    public static function saveImage(UploadedFile $file, $model, string $attribute = 'image'): bool
    {
        $model->$attribute = Storage::disk('public')->put($model->getTable(), $file);
        if ($model->getRawOriginal($attribute)){
            Storage::disk('public')->delete($model->getRawOriginal($attribute));
        }
        return $model->save();
    }

    public static function deleteImage($path): bool
    {
        return Storage::disk('public')->delete($path);
    }
}
