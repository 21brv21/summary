<?php

namespace App\Traits\Models;

use App\Models\Ref\RefSlug;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasSlugTrait
{
    public function slug(): BelongsTo
    {
        return $this->belongsTo(RefSlug::class);
    }

    public static function deleteSlug(string $className, string $modelId)
    {
        return RefSlug::where('model', $className)->where('model_id', $modelId)->delete();
    }

    public function getSlugAttribute(): ?string
    {
        return $this->slug()->first()->slug ?? null;
    }

    public static function saveSlug(string $slug, object $model): object
    {
        $className = get_class($model);
        $refSlug = RefSlug::updateOrCreate(
            ['model_type' => $className, 'model_id' => $model->id],
            ['model_type' => $className, 'model_id' => $model->id, 'slug' => $slug]
        );

        $model->slug_id = $refSlug->id;
        $model->save();
        return $model;
    }
}
