<?php

namespace App\Providers;

use App\Library\Localization;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/admin/dashboard';

    public function boot()
    {
        $this->configureRateLimiting();
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapAdminRoutes();
        $this->mapAdminApiRoutes();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->prefix(Localization::configPrefix())
                ->group(base_path('routes/web.php'));
        });

    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }


    protected function mapWebRoutes()
    {
        foreach (glob(base_path('routes/web/*.php')) as $routeFile) {
            Route::group([
                'prefix' => Localization::configPrefix(),
                'middleware' => ['web'],
            ], $routeFile);
        }
    }


    protected function mapApiRoutes()
    {
        foreach (glob(base_path('routes/api/*.php')) as $routeFile) {
            $filePrefix = basename($routeFile, ".php");
            Route::group([
                'prefix' => "api/v1/{$filePrefix}",
                'middleware' => ['api'],
            ], $routeFile);
        }
    }


    protected function mapAdminRoutes()
    {
        foreach (glob(base_path('routes/admin/*.php')) as $routeFile) {
            Route::group([
                'prefix' => "admin",
                'middleware' => ['web', 'admin'],
            ], $routeFile);
        }
    }


    protected function mapAdminApiRoutes()
    {
        foreach (glob(base_path('routes/apiAdmin/*.php')) as $routeFile) {
            $filePrefix = basename($routeFile, ".php");
            Route::group([
                'prefix' => "admin/api/v1/{$filePrefix}",
                'middleware' => ['api'],
            ], $routeFile);
        }
    }

}
