<?php

namespace App\Services\Web\Pages;

use App\Library\Localization;
use App\Library\SeoSchema\SeoSchema;
use App\Models\Pages\Blog;
use App\Models\Pages\Page;
use App\Models\Products\Product;
use App\Models\Redirects\Redirect;
use App\Models\Ref\RefSlug;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Nette\Schema\ValidationException;

class PagesService
{

    public static function make()
    {
        return new static();
    }

    public function slug(string $slug)
    {
        $slug = RefSlug::getModelBySlug($slug);

        if (empty($slug)){
            throw new ValidationException(trans('crud.errorsCode.404'));
        }

        $result = '';

        switch ($slug->model_type) {
            case Product::class:
                $result = $this->product($slug);
                break;
            case Page::class:
                $result = $this->page($slug);
                break;
            case Blog::class:
                $result = $this->blog($slug);
                break;
            case Redirect::class:
                $result = $this->redirect($slug);
                break;
        }

        return $result;
    }

    private function product(RefSlug $slug): View
    {
        $product = Product::where('id', $slug->model_id)
            ->with(['translation', 'images', 'reviews'])
            ->first();

        $images = $product->images->pluck('url')->toArray();

        array_unshift($images, $product->image);

        $seoSchemas = SeoSchema::getProductSchemas($product);

        return view('web.products.show', compact('product','images', 'seoSchemas'));
    }

    private function page(RefSlug $slug)
    {
        $slug = RefSlug::getModelBySlug($slug->slug);
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getPageSchemas($page);

        return view('web.pages.' . $page->template, compact('page', 'seoSchemas'));
    }

    private function blog(RefSlug $slug)
    {
        $blog = Blog::getBlogBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getBlogSchemas($blog);
        return view('web.pages.article', compact('blog', 'seoSchemas'));
    }

    private function redirect(RefSlug $slug): RedirectResponse
    {
        $redirect = Redirect::find($slug->model_id);
        return redirect(route('web.slug', ['slug' => $redirect->redirect_to]));
    }
}
