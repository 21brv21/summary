<?php

namespace App\Services\Telegram;

use App\Models\Orders\Order;
use App\Models\Products\Review;
use App\Models\WebForms\WebForm;
use Telegram\Bot\Api;

class TelegramService
{
    private function createTelegramBot(): Api
    {
        return new Api(config('app.connection.telegram_token'));
    }

    public function send(string $message)
    {
        $telegram = $this->createTelegramBot();
        return $telegram->sendMessage([
            'chat_id' => config('app.connection.telegram_chat_id'),
            'text' => $message,
            'parse_mode' => 'HTML',
        ]);
    }

    public function sendReview(Review $review)
    {
        $text = $this->generateHtmlReviewBody($review);
        $telegram = $this->createTelegramBot();
        return $telegram->sendMessage([
            'chat_id' => config('app.connection.telegram_chat_id'),
            'text' => $text,
            'parse_mode' => 'HTML',
        ]);
    }

    public function sendSms(string $text)
    {
        $telegram = $this->createTelegramBot();
        return $telegram->sendMessage([
            'chat_id' => config('app.connection.telegram_chat_id'),
            'text' => $this->generateHtmlSmsBody($text),
            'parse_mode' => 'HTML',
        ]);
    }


    private function generateHtmlWebBody(WebForm $webForm): string
    {
        $text = is_null($webForm->theme) ? "Новая заявка с сайта" : $webForm->theme;
        $text = "<b>".$text."</b>";
        $text = "📞  ".$text."\n\n"
            . "Имя: " . $webForm->name . "\n"
            . "Телефон: " . formattedPhone($webForm->phone) . "\n";

        if ($webForm->message) $text = $text . "Сообщение: " . $webForm->message . "\n";

        return $text;
    }


    private function generateHtmlOrderBody(Order $order): string
    {
        $text = "<b>Новый заказ с сайта</b>\n\n";
        $user = $order->user()->first();
        $products = $order->products()->get();



        if (!empty($order->address_recipient)){
            $text = $text   . "Адрес доставки: " . $order->deliveryType()->first()->name . "\n";
        }

        $text = $text . "Сумма заказа: " . formattedPhone($order->total_price) . " грн \n\n";

        foreach ($products as $index => $product)
        {
            $text = $text.'•'.$product->name."(×".$product->pivot->quantity.")";
            $text = count($products) - 1 != $index ? $text.", \n" : $text."\n\n";
        }


        if ($order->comment) $text = $text . "Комментарий: " . $order->comment . "\n";

        return $text;
    }


    private function generateHtmlOrderInfoBody(Order $order): string
    {
        $text = "<b>Информация о заказе</b>\n\n";
        $user = $order->user()->first();
        $products = $order->products()->get();

        $text = "ℹ️ ".$text
            . "Имя: " . $user->full_name . "\n"
            . "Телефон: " . formattedPhone($user->phone) . "\n"
            . "Тип оплаты: " . $order->paymentType()->first()->name . "\n"
            . "Тип доставки: " . $order->deliveryType()->first()->name . "\n"
            . "Номер получателя: " . $order->full_name_recipient . "\n"
            . "Телефон получателя: " . $order->phone_recipient . "\n"
            . "Адрес доставки: " . $order->address_recipient . "\n"
            . "Сумма заказа: " . formattedPhone($order->total_price) . " грн \n\n";

        foreach ($products as $index => $product)
        {
            $text = $text.'•'.$product->name."(×".$product->pivot->quantity.")";
            $text = count($products) - 1 != $index ? $text.", \n" : $text."\n\n";
        }


        if ($order->comment) $text = $text . "Комментарий: " . $order->comment . "\n";

        return $text;
    }

    private function generateHtmlSmsBody(string $text): string
    {
        return "✉️ ".$text;
    }

    private function generateHtmlReviewBody(Review $review): string
    {
        $stars = '';
        switch ($review->rating) {
            case 0:
                $stars = '0️⃣';
                break;
            case 1:
                $stars = '⭐️';
                break;
            case 2:
                $stars = '⭐️⭐️';
                break;
            case 3:
                $stars = '⭐️⭐️⭐️';
                break;
            case 4:
                $stars = '⭐️⭐️⭐️⭐️';
                break;
            case 5:
                $stars = '⭐️⭐️⭐️⭐️⭐️';
                break;
        }

        $text = "🔔 <b>Новый отзыв о товаре!</b>\n\n"
            . "Имя: " . $review->name . "\n"
            . "Оценка: " . $stars . "\n"
            . "Отзыв: " . $review->description . "\n";

        return $text;
    }
}
