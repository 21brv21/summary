<?php

namespace App\Services\ApiAdmin\Products;

use App\Http\Requests\ApiAdmin\Products\ApiProductsSearchRequest;
use App\Models\Categories\Category;
use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class ApiProductsService
{
    public static function make()
    {
        return new static();
    }

    public function search(ApiProductsSearchRequest $request):Collection
    {
        $search = $request->get('search') ?? null;
        $products = Product::query();
        if (!is_null($search)){
            $products = $products->whereHas('translations', function (Builder $query) use ($search) {
                $query->where('name', 'LIKE','%'.$search.'%');
            })->orWhere('cod', 'LIKE','%'.$search.'%');
        }
        return $products->limit(50)->orderBy('updated_at')->get();
    }

    public function searchByCategory(int $id,ApiProductsSearchRequest $request):Collection
    {
        $search = $request->get('search') ?? null;
        $products = Product::whereHas('categories', function (Builder $query) use ($id) {
            $query->where('products_categories.category_id', $id);
        });
        if (!is_null($search)){
            $products = $products->whereHas('translations', function (Builder $query) use ($search) {
                $query->where('name', 'LIKE','%'.$search.'%');
            })->orWhere('cod', 'LIKE','%'.$search.'%');
        }
        return $products->limit(50)->orderBy('updated_at')->get();
    }
}
