<?php

namespace App\Services\ApiAdmin\Orders;

use App\Http\Requests\ApiAdmin\Products\ApiProductsSearchRequest;
use App\Models\Orders\Order;
use App\Models\Products\Product;
use App\Models\Ref\RefStatus;

class ApiOrdersService
{
    public static function make()
    {
        return new static();
    }

    public function setPaymentStatus(Order $order)
    {
        $status = RefStatus::getPaymentOrderStatus();
        return $order->update(['status_id' =>  $status->id]);
    }

    public function setCompletedStatus(Order $order)
    {
        $status = RefStatus::getCompletedOrderStatus();

        $products = $order->products()->get()->pluck('pivot.quantity', 'pivot.product_id')->toArray();
        Product::syncProductsQuantity($products, []);

        return $order->update(['status_id' =>  $status->id]);
    }

    public function setCanceledStatus(Order $order)
    {
        $statusCanceled = RefStatus::getCanceledOrderStatus();
        $statusCompleted = RefStatus::getCompletedOrderStatus();

        if ($statusCompleted->id == $order->status_id){
            $oldProducts = $order->products()->get()->pluck('pivot.quantity', 'pivot.product_id')->toArray();
            Product::syncProductsQuantity([], $oldProducts);
        }

        return $order->update(['status_id' =>  $statusCanceled->id]);
    }
}
