<?php

namespace App\Services\Crud;

trait Filters
{
    public $filters = [];

    public function addFilter(CrudFilter $filter)
    {
        $this->filters[$filter->name] = $filter;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function createFilter(array $options, $values = false, $logic = false)
    {
        $this->checkFiltersOptions($options);

        $filter = new CrudFilter();
        $filter->setName($options['name']);
        $filter->setPlaceholder($options['placeholder']);
        $filter->setLogic($logic);

        if (isset($options['type'])) $filter->setType($options['type']);
        if (isset($options['class'])) $filter->setClass($options['class']);
        if (isset($options['keyReduce'])) $filter->setKeyReduce($options['keyReduce']);
        if (is_callable($values)) $filter->setValues($values());

        $this->addFilter($filter);
    }


    public function checkFiltersOptions(array $options)
    {
        if (!isset($options['name'])) {
            abort(500, trans('crud.errorsFilters.name'));
        }
        if (!isset($options['placeholder'])) {
            abort(500, trans('crud.errorsFilters.placeholder'));
        }
    }
}


class CrudFilter
{
    const FILTER_SELECT2 = 'select2-filter';
    const FILTER_SELECT2_KEY = 'select2-key-filter';
    const FILTER_SEARCH_API = 'search-api-filter';

    public $logic;
    public string $name;
    public string $placeholder;
    public array $values = [];
    public string $classes;
    public string $type;
    public string $class;
    public string $keyReduce;


    public function __construct()
    {
        $this->type = CrudFilter::FILTER_SELECT2;
        $this->class = 'col-md-3';
        $this->keyReduce = 'id';
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setPlaceholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
    }

    public function setValues(array $values)
    {
        $this->values = $values;
    }

    public function setLogic($logic)
    {
        $this->logic = $logic;
    }

    public function setClasses(string $classes)
    {
        $this->classes = $classes;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setClass(string $class)
    {
        $this->class = $class;
    }

    public function setKeyReduce(string $keyReduce)
    {
        $this->keyReduce = $keyReduce;
    }
}
