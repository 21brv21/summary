<?php

namespace App\Services\Crud;

trait Columns
{
    public array $columns = [];

    public function addColumn(CrudColumn $column)
    {
        $this->columns[$column->name] = $column;
    }

    public function getColumns():array
    {
        return $this->columns;
    }

    public function createColumn(array $options)
    {
        $this->checkColumnsOptions($options);

        $column = new CrudColumn();
        $column->setName($options['name']);
        $column->setLabel($options['label']);

        if (isset($options['type'])) $column->setType($options['type']);
        if (isset($options['sort'])) $column->setSort($options['sort']);
        if (isset($options['length'])) $column->setLength($options['length']);
        if (isset($options['classes'])) $column->setClasses($options['classes']);

        $this->addColumn($column);
    }

    public function checkColumnsOptions(array $options)
    {
        if (!isset($options['name'])) {
            abort(500, trans('crud.errorsColumns.name'));
        }
        if (!isset($options['label'])) {
            abort(500, trans('crud.errorsColumns.label'));
        }
    }
}


class CrudColumn
{
    //DEFAULT
    const COLUMN_TEXT = 'text-column';
    const COLUMN_MULTIPLE = 'multiple-column';
    const COLUMN_BOOLEAN = 'boolean-column';
    const COLUMN_COLOR = 'color-column';
    const COLUMN_PHONE = 'phone-column';
    const COLUMN_NUMBER = 'number-column';
    const COLUMN_IMAGE = 'image-column';

    //CUSTOM
    const COLUMN_GRN = 'grn-column';
    const COLUMN_EURO = 'euro-column';
    const COLUMN_SET_STATUS = 'set-status-column';

    public string $name;
    public string $label;
    public string $type;
    public int $length;
    public string $classes;
    public bool $sort;

    public function __construct()
    {
        $this->type = CrudColumn::COLUMN_TEXT;
        $this->sort = false;
        $this->length = 50;//довжина обмеження стовбця
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setSort(bool $sort)
    {
        $this->sort = $sort;
    }

    public function setLength(int $length)
    {
        $this->length = $length;
    }

    public function setClasses(string $classes)
    {
        $this->classes = $classes;
    }
}
