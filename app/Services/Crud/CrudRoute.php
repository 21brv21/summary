<?php

namespace App\Services\Crud;

use Illuminate\Support\Facades\Route;

class CrudRoute
{
    public static function resource(string $name, string $controller)
    {
        Route::get($name,  [$controller, 'index'])->name('admin.'.$name);
        Route::get($name.'/search',  [$controller, 'search']);
        Route::get($name.'/create',  [$controller, 'create'])->name('admin.'.$name.'.create');
        Route::get($name.'/{id}',  [$controller, 'show'])->name('admin.'.$name.'.show');
        Route::get($name.'/{id}/edit',  [$controller, 'edit']);
        Route::post($name.'/store',  [$controller, 'store']);
        Route::put($name.'/update/{id}',  [$controller, 'update']);
        Route::delete($name.'/delete/{id}',  [$controller, 'destroy']);
    }
}
