<?php

namespace App\Services\Crud;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait CrudTranslationTrait
{

    public static function hasTranslationTrait(): bool
    {
        return true;
    }

    public function translations(): HasMany
    {
        return $this->hasMany(get_class($this) . config('crud.translation_suffix'), $this->translationForeignKey);
    }

    public function translation(): HasOne
    {
        return $this->hasOne(get_class($this) . config('crud.translation_suffix'), $this->translationForeignKey)->where('locale', app()->getLocale());

    }

    public function getTranslationForeignKey(): string
    {
        return $this->translationForeignKey;
    }

    public function getTranslationModelName(): string
    {
        return get_class($this) . config('crud.translation_suffix');
    }
}
