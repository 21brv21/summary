<?php

namespace App\Services\Crud;

trait Buttons
{
    public array $buttons = [];

    public function addButton(CrudButton $button)
    {
        $this->buttons = array($button->name => $button) + $this->buttons;
    }

    public function getButtons(): array
    {
        return $this->buttons;
    }

    public function createButton(array $options)
    {
        $this->checkButtonOptions($options);

        $button = new CrudButton();
        $button->setName($options['name']);
        $button->setLabel($options['label']);
        $button->setType($options['type']);

        if (isset($options['link'])) $button->setLink($options['link']);
        if (isset($options['linkClass'])) $button->setLinkClass($options['linkClass']);
        if (isset($options['iconClass'])) $button->setIconClass($options['iconClass']);

        $this->addButton($button);
    }

    public function checkButtonOptions(array $options)
    {
        if (!isset($options['name'])) {
            abort(500, trans('crud.errorsButtons.name'));
        }
        if (!isset($options['label'])) {
            abort(500, trans('crud.errorsButtons.label'));
        }
        if (!isset($options['type'])) {
            abort(500, trans('crud.errorsButtons.type'));
        }
    }
}


class CrudButton
{
    const BUTTON_SHOW = 'show-button';
    const BUTTON_EDIT = 'edit-button';
    const BUTTON_DELETE = 'delete-button';
    const BUTTON_LINK = 'link-button';
    const BUTTON_CLICK = 'click-button';

    public string $name;
    public string $label;
    public string $type;
    public string $link;
    public string $linkClass;
    public string $iconClass;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setLink(string $link)
    {
        $this->link = $link;
    }

    public function setLinkClass(string $linkClass)
    {
        $this->linkClass = $linkClass;
    }

    public function setIconClass(string $iconClass)
    {
        $this->iconClass = $iconClass;
    }
}
