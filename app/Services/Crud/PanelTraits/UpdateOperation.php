<?php

namespace App\Services\Crud\PanelTraits;

use App\Library\Localization;
use Illuminate\Http\Request;
use Inertia\Inertia;

trait UpdateOperation
{
    public function edit($id)
    {
        if (!$this->crud->getAllowUpdate()) abort(403, trans('crud.errorsViews.access'));
        $this->crud->setOperation('edit');
        return Inertia::render('Admin/' . $this->crud->getViewEdit(), [
            'options' => $this->crud->getOptions(),
            'fields' => $this->crud->getFields(),
            'fieldsTranslations' => $this->crud->getFieldsTranslations(),
            'entry' => $this->crud->getModelEdit()->find($id),
            'languages' => getLanguages(),
        ]);
    }

    public function update($id, Request $request)
    {
        if (!$this->crud->getAllowUpdate()) abort(403, trans('crud.errorsViews.access'));
        $this->crud->setOperation('update');
        $this->crud->entry = $this->crud->getModel()->find($id);
        $data = $request->validate($this->crud->getValidateRules(), $this->crud->getValidateMessages());
        $data =$this->filteredDataRequest($data);
        $this->crud->entry->update($data);
        $this->syncTranslation($request);
        $this->saveFiles($request, $data);
        $this->syncRelations($data);

        $url = $this->redirectAfterSave();

        return redirect($url)->with([
            'flash' => [
                'success' => 'Успішно оновлено'
            ]
        ]);
    }

}
