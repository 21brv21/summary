<?php

namespace App\Services\Crud\PanelTraits;

trait DeleteOperation
{
    public function destroy($id)
    {
        if (!$this->crud->getAllowDelete()) abort(403, trans('crud.errorsViews.access'));
        $this->crud->setOperation('delete');
        $this->crud->entry = $this->crud->getModel()->find($id);
        $files = $this->getFiles();
        $this->crud->entry->delete();
        session()->flash('success','Успішно видалено');
        return $this->deleteFiles($files);

//        return back()->with([
//            'flash' => [
//                'success' => 'Успішно видалено'
//            ]
//        ]);
    }
}
