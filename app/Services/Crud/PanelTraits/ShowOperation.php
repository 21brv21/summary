<?php

namespace App\Services\Crud\PanelTraits;

use Inertia\Inertia;

trait ShowOperation
{
    public function show($id)
    {
        if (!$this->crud->getAllowShow()) abort(403, trans('crud.errorsViews.access'));
        $this->crud->setOperation('show');
        return Inertia::render('Admin/' . $this->crud->getViewShow(), [
            'options' => $this->crud->getOptions(),
            'fields' => $this->crud->getFields(),
            'entry' => $this->crud->getModel()->find($id),
        ]);
    }
}
