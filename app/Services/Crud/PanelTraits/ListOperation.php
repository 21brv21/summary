<?php

namespace App\Services\Crud\PanelTraits;

use App\Models\Ref\RefSetting;
use Inertia\Inertia;

trait ListOperation
{
    public function index()
    {
        $this->crud->setOperation('list');
        return Inertia::render('Admin/' . $this->crud->getViewIndex(),[
            'options' => $this->crud->getOptions(),
            'columns' => $this->crud->getColumns(),
            'buttons' => $this->crud->getButtons(),
            'filters' => $this->crud->getFilters(),
        ]);
    }

    public function search()
    {
        $this->crud->setOperation('list');
        $query = $this->crud->getQuerySearch();
        $query = $this->setOrderBy($query);
        $query = $this->setFilters($query);
        return $query->paginate(RefSetting::PAGINATION_ADMIN);
    }


    private function setOrderBy($query)
    {
        $orderBy = request()->get('orderBy') ?? null;
        $orderType = request()->get('orderType') ?? null;
        $orderType = $orderType == 'true' ? 'desc' : 'asc';
        $columns = $this->crud->getColumns();
        if(isset($columns[$orderBy]) && $columns[$orderBy]->sort){
            $query = $query->orderBy($orderBy, $orderType);
        }
        return $query;
    }

    private function setFilters($query)
    {
        $filters = $this->crud->getFilters();
        foreach ($filters as $filter) {
            $value = request()->get($filter->name);
            if ($value && is_callable($filter->logic)) {
                ($filter->logic)($value);
            }
        }
        return $query;
    }
}
