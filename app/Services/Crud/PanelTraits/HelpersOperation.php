<?php

namespace App\Services\Crud\PanelTraits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait HelpersOperation
{
    public function filteredDataRequest(array $data): array
    {
        if ($this->crud->getIsTranslation()) {
            unset($data['translations']);
        }
        return $data;
    }

    public function syncTranslation(Request $request)
    {
        if ($this->crud->getIsTranslation()) {
            $this->crud->entry->translations()->delete();
            $translationModelName = $this->crud->entry->getTranslationModelName();
            $translationForeignKey = $this->crud->entry->getTranslationForeignKey();
            $translationForeignKeyValue = $this->crud->entry->{$this->crud->getKeyName()};
            foreach ($request->get('translations') as $lang => $item) {
                $item[$translationForeignKey] = $translationForeignKeyValue;
                $item['locale'] = $lang;
                $translationModelName::updateOrCreate([
                    $translationForeignKey => $translationForeignKeyValue,
                    'locale' => $lang,
                ], $item);
            }
        }

    }

    public function syncRelations(array $data)
    {
        $relations = $this->crud->getRelationsMany();
        foreach ($relations as $relation) {
            if (isset($data[$relation])) {
                $this->crud->entry->$relation()->sync($data[$relation]);
            }
        }
    }

    public function getFiles(): array
    {
        $files = [];
        if (isset($this->crud->entry->image)) {
            $files[] = $this->crud->entry->getRawOriginal('image');
        }
        return $files;
    }

    public function saveFiles(Request $request, array $data): array
    {
        if (count($request->allFiles()) > 0) {
            if ($request->hasFile('image')) {
                $this->crud->entry::saveImage($request->file('image'), $this->crud->entry, 'image');
            }
            if ($request->hasFile('image_white')) {
                $this->crud->entry::saveImage($request->file('image_white'), $this->crud->entry, 'image_white');
            }
        }
        return $data;
    }

    public function deleteFiles(array $files)
    {
        $deletedFiles = 0;
        foreach ($files as $path) {
            Storage::disk('public')->delete($path);
            $deletedFiles++;
        }
        return $deletedFiles > 0;
    }

    public function redirectAfterSave(): string
    {
        return $this->crud->getRoute();
//        return request()->session()->has('redirectAfterUpdate') ? request()->session()->get('redirectAfterUpdate') : $this->crud->getRoute();
    }

}
