<?php

namespace App\Services\Crud\PanelTraits;

use App\Library\Localization;
use Illuminate\Http\Request;
use Inertia\Inertia;

trait CreateOperation
{
    public function create()
    {
        if (!$this->crud->getAllowCreat()) abort(403, trans('crud.errorsViews.access'));
        $this->crud->setOperation('create');
        return Inertia::render('Admin/' . $this->crud->getViewCreate(), [
            'options' => $this->crud->getOptions(),
            'fields' => $this->crud->getFields(),
            'fieldsTranslations' => $this->crud->getFieldsTranslations(),
            'languages' => getLanguages(),
        ]);
    }

    public function store(Request $request)
    {
        $this->crud->setOperation('store');
        $data = $request->validate($this->crud->getValidateRules(), $this->crud->getValidateMessages());
        $data =$this->filteredDataRequest($data);
        $this->crud->entry = $this->crud->getModel()->create($data);
        $this->syncTranslation($request);
        $this->saveFiles($request, $data);
        $this->syncRelations($data);

        $url = $this->redirectAfterSave();

        return redirect($url)->with([
            'flash' => [
                'success' => 'Успішно створено'
            ]
        ]);
    }
}
