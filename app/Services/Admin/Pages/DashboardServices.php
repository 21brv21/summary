<?php

namespace App\Services\Admin\Pages;

use App\Models\Orders\Order;
use App\Models\Products\Product;
use App\Models\Ref\RefStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class DashboardServices
{
    public static function make()
    {
        return new static();
    }

    public function getSalesGrph(): array
    {
        $orders = Order::where('updated_at', '>', now()->subYear())
            ->where('status_id', '!=', RefStatus::getNewOrderStatus()->id)
            ->get();

        $data = [];
        foreach ($orders as $order) {
            $month = Carbon::parse($order->created_at)->month;
            if (!isset($data[$month])) $data[$month] = 0;
            $data[$month] = $data[$month] + $order->total_price;
        }

        $result = [[trans('admin.month'), trans('admin.sales')]];
        foreach ($data as $monthNumber => $total){
            array_push($result,[genShortMonthNameByNumber($monthNumber), $total]);
        }
        return $result;
    }

    public function getTotalInvoices(Collection $orders)
    {
        return $orders->count();
    }

    public function getOrdersPriceInfo(Collection $orders)
    {
        return array_sum($orders->pluck('total_price')->toArray());
    }

    public function getTotalPriceProducts()
    {
        $products = Product::getProductsAll();
        $totalPrice = 0;
        foreach ($products as $product) {
            $quantity = $product->quantity > 0 ? $product->quantity : 0;
            $totalPrice += $product->price * $quantity;
        }

        return $totalPrice;
    }
}
