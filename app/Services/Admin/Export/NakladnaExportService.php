<?php


namespace App\Services\Admin\Export;

use App\Models\Orders\Order;
use App\Services\Admin\Export\Sheets\NakladnaSheetService;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class NakladnaExportService implements WithMultipleSheets
{
    use Exportable;

    protected Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }


    public function sheets(): array
    {
        return [
            new NakladnaSheetService($this->order),
        ];
    }
}
