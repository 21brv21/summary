<?php

namespace App\Services\Admin\Export\Sheets;

use App\Models\Orders\Order;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class NakladnaSheetService implements FromView, WithTitle
{
    use Exportable;

    protected Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function view(): View
    {
        $this->order = Order::checkNakladnaNumber($this->order);
        $data = Order::getInfoForDocument($this->order);
        return view('excel.orders.nakladna', compact('data'));
    }

    public function title(): string
    {
        return 'Накладна';
    }
}
