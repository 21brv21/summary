<?php

namespace App\Services\Admin\Export\Sheets;

use App\Models\Supplies\Supply;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use stdClass;

class SuppliesSheetService implements FromView, WithTitle
{
    use Exportable;

    protected Supply $supply;

    public function __construct(Supply $supply)
    {
        $this->supply = $supply;
    }

    public function view(): View
    {
        $data = $this->filterForView(Supply::getInfoForDocument($this->supply));
        $supply = $this->supply;
        return view('excel.supplies.zakaz', compact('data', 'supply'));
    }

    public function title(): string
    {
        return 'zakaz';
    }

    private function filterForView(array $data): array
    {
        $categories = [];
        $products = [];

        foreach ($data['categories'] as $category)
        {
            $new = new stdClass();
            $new->category = $category;
            $new->products = [];
            $categories[$category['id']] = $new;
        }

        foreach ($data['products'] as $product)
        {
            $newProduct = new stdClass();
            $newProduct->name = $product['name'];
            $newProduct->cod = $product['cod'];
            $newProduct->quantity = $product['pivot']['quantity'];
            $newProduct->price = $product['pivot']['price'];
            array_push($categories[$product['pivot']['category_id']]->products, $newProduct);
        }

        foreach ($categories as $category)
        {
            foreach ($category->products as $key => $product)
            {
                $product->rowspan = $key == 0 ? count($category->products) : 0;
                $product->categoryName = $category->category['name'];

                array_push($products, $product);
            }
        }

        $data['products'] = $products;

        return $data;
    }

}
