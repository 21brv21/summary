<?php


namespace App\Services\Admin\Export;

use App\Models\Orders\Order;
use App\Models\Supplies\Supply;
use App\Services\Admin\Export\Sheets\NakladnaSheetService;
use App\Services\Admin\Export\Sheets\SuppliesSheetService;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SuppliesExportService implements WithMultipleSheets
{
    use Exportable;

    protected Supply $supply;

    public function __construct(Supply $supply)
    {
        $this->supply = $supply;
    }


    public function sheets(): array
    {
        return [
            new SuppliesSheetService($this->supply),
        ];
    }
}
