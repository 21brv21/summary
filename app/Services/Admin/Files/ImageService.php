<?php

namespace App\Services\Admin\Files;

use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImageService
{
    private const TYPES = [
        'products' => Product::class,
    ];

    public function getModel(string $modelName): Model
    {
        $model = self::TYPES[$modelName] ?? throw new NotFoundHttpException(trans('validation.incorrectType'));
        return app()->make($model);
    }

    public function uploadFile(UploadedFile $file, string $folder): array
    {
        $data['name'] = $file->getClientOriginalName();
        $data['path'] = $file->store($folder, 'public');
        $data['size'] = $file->getSize();
        $data['mime_type'] = $file->getMimeType();
        return $data;
    }
}
