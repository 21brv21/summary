<?php

namespace App\Services\Api\Orders;

use App\Jobs\SendToTelegram;
use App\Models\Orders\Order;
use App\Models\Ref\RefSetting;

class ApiOrderServices
{
    public function __construct(private Order $order)
    {
    }

    public function store(array $data): array
    {
        $data['bottle_price'] = RefSetting::getBottlePrice($data['bottle_quantity'], $data['is_new_user']);
        $data['outpost_price'] = RefSetting::getSettingByKey(RefSetting::SETTING_OUTPOST_PRICE);
        $data['total_price'] = $this->calculateTotalPrice($data);

        $order = $this->order->create($data);
        $order->products()->attach($this->convertProducts($data));

        SendToTelegram::dispatch($this->generateTelegramMessage($order));

        return [
            'order' => $order->load('products'),
            'total' => $order->total_price,
            'ipn' => getSetting(RefSetting::SETTING_PAYMENT_IPN),
            'mfo' => getSetting(RefSetting::SETTING_PAYMENT_MFO),
            'rr' => getSetting(RefSetting::SETTING_PAYMENT_RR),
            'bank' => getSetting(RefSetting::SETTING_PAYMENT_BANK),
            'name' => getSetting(RefSetting::SETTING_PAYMENT_NAME),
        ];
    }

    private function generateTelegramMessage(Order $order): string
    {
        $isNewUser = $order->is_new_user ? 'Так' : 'Ні';
        $order->load('products.translation');
        $text = "🔥️ Замовлення з сайту №" . $order->id . "\n"
            . "========Контакти========\n"
            . "Імя: " . $order->name . "\n"
            . "Телефон: " . formattedPhone($order->phone) . "\n"
            . "Адреса доставки: " . $order->address . "\n"
            . "Новий користувач: " . $isNewUser . "\n"
            . "=======Замовлення=======\n"
            . "Кількість бутлів: " . $order->bottle_quantity . " (" . $order->bottle_price . " грн)\n"
            . "Кількість застави: " . $order->outpost_quantity . " (по " . $order->outpost_price . " грн)\n";

        foreach ($order->products as $product) {
            if ($product->pivot->quantity > 0) {
                $text .= $product->translation->name . ": " . $product->pivot->quantity . " (по " . $product->pivot->price . " грн)\n";
            }
        }
        return $text . "Всього: " . $order->total_price . " грн\n";
    }

    private function calculateTotalPrice($data): int
    {
        $total = $data['bottle_price'] + $data['outpost_price'] * $data['outpost_quantity'];

        foreach ($data['products'] as $product) {
            $total += $product['price'] * $product['quantity'];
        }

        return $total;
    }

    private function convertProducts(array $data): array
    {
        foreach ($data['products'] as $product) {
            $products[$product['id']] = [
                'price' => $product['price'],
                'quantity' => $product['quantity'],
            ];
        }

        return $products ?? [];
    }
}
