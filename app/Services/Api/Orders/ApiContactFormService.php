<?php

namespace App\Services\Api\Orders;

use App\Models\Orders\ContactForm;

class ApiContactFormService
{
    public function __construct(private ContactForm $contactForm){}

    public function store(array $data): ContactForm
    {
        return $this->contactForm->create($data);
    }
}
