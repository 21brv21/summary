<?php


namespace App\Services\Sms;

use App\Models\Orders\Order;
use App\Models\Users\Admin;
use App\Models\Users\Customer;
use GuzzleHttp\Client as GuzzleClient;

class SmsService
{
    public static function make()
    {
        return new static();
    }

    public function sendSms($recipient, $sms):void
    {
        $this->send($recipient, $sms);
    }

    public function sendOrderAdmins(Order $order, Customer $client)
    {
        $sms = "Заказ: ".$client->name." ,".$client->phone." Товар:";
        $products = $order->products()->get();
        foreach ($products as $key => $product)
        {
            $sms .= $product->name;

            if ($key != count($products) - 1) $sms .=', ';
        }

        $admins = Admin::getAllAdminsNotificationPhone();
        foreach ($admins as $admin)
        {
            $this->send($admin->phone, $sms);
        }
    }

    public function generateXmlBody($recipient, $sms): string
    {
        $xmlPath = app_path() . '/Services/Sms/xmlBody.xml';
        $xmlBody = file_get_contents($xmlPath);

        $xmlBody = str_replace('__ALPHASMS__KEY__', config('app.connection.alphasms_key'), $xmlBody);
        $xmlBody = str_replace('__RECIPIENT__', $recipient, $xmlBody);
        $xmlBody = str_replace('__MSG__', $sms, $xmlBody);

        return $xmlBody;
    }

    private function send($recipient, $sms):void
    {
        $client = new GuzzleClient();
        $xmlBody = $this->generateXmlBody($recipient, $sms);
        $client->request('POST', 'http://alphasms.ua/api/xml.php', [
            'headers' => [
                'Content-Type' => 'text/xml; charset=UTF8'
            ],
            'body' => $xmlBody
        ]);
    }
}
