<?php

namespace App\Models\Files;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'path',
        'mime_type',
        'size',
    ];

    protected $appends = [
        'url',
    ];

    protected $hidden = [
        'imageable_type',
        'imageable_id'
    ];

    public static function boot(): void
    {
        parent::boot();

        self::deleting(function ($model) {
            Storage::disk('public')->delete($model->path);
        });
    }

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }

    public function getUrlAttribute(): string
    {
        return Storage::disk('public')->url($this->attributes['path']);
    }
}
