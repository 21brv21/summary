<?php

namespace App\Models\Ref;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class RefSetting extends Model
{
    const PAGINATION_ADMIN = 25;
    const PAGINATION_WEB = 24;

    const SEO_TITLE_MAX = 155;
    const SEO_DESCRIPTION_MAX = 255;

    const SETTING_PHONE_VODAFONE = 'phone_vodafone';
    const SETTING_PHONE_KIEVSTAR = 'phone_kievstar';
    const SETTING_PHONE_LIFE = 'phone_life';
    const SETTING_PHONE_CITY = 'phone_city';
    const SETTING_TELEGRAM = 'telegram';
    const SETTING_VIBER = 'viber';
    const SETTING_FACEBOOK = 'facebook';

    const SETTING_PRICE_FIRST = 'price_first';//за бутль
    const SETTING_PRICE_SECOND = 'price_second';//ціна за бутль від 3 до 7
    const SETTING_PRICE_THIRD = 'price_third';//ціна за бутль від 8

    const SETTING_SALE_PRICE_FIRST = 'sale_price_first';//акція для перших кліентів
    const SETTING_SALE_PRICE_SECOND = 'sale_price_second';//акція для перших кліентів за бутль від 3 до 7
    const SETTING_SALE_PRICE_THIRD = 'sale_price_third';//акція для перших кліентів від 10

    const SETTING_OUTPOST_PRICE = 'outpost_price';//Заставна вартість
    const SETTING_CERTIFICATE = 'certificate';
    const SETTING_PAYMENT_IPN = 'payment_ipn';
    const SETTING_PAYMENT_RR = 'payment_rr';
    const SETTING_PAYMENT_BANK = 'payment_bank';
    const SETTING_PAYMENT_MFO = 'payment_mfo';
    const SETTING_PAYMENT_NAME = 'payment_name';

    protected $table = 'ref_settings';

    protected $fillable = [
        'name',
        'key',
        'value',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public static function getSettingByKey(string $key)
    {
        $name = $key.SETTING_BY_KET_CACHE;
        if (!Cache::has($name)) {
            $value = RefSetting::where('key', 'LIKE',$key)->first();
            Cache::forever($name, $value->value ?? null);
        }
        return Cache::get($name);
    }

    public static function getBottlePrice(int $quantity, bool $isNewUser): int
    {
        $totalPrice = 0;
        if ($isNewUser) {
            if ($quantity < 3) {
                $totalPrice += getSetting(self::SETTING_SALE_PRICE_FIRST) * $quantity;
            } else if ($quantity < 10) {
                $totalPrice += getSetting(self::SETTING_SALE_PRICE_SECOND) * $quantity - getSetting(self::SETTING_SALE_PRICE_SECOND);
            } else {
                $totalPrice += getSetting(self::SETTING_SALE_PRICE_THIRD) * $quantity;
            }
        } else {
            if ($quantity < 3) {
                $totalPrice += getSetting(self::SETTING_PRICE_FIRST) * $quantity;
            } else if ($quantity < 8) {
                $totalPrice += getSetting(self::SETTING_PRICE_SECOND) * $quantity;
            } else {
                $totalPrice += getSetting(self::SETTING_PRICE_THIRD) * $quantity;
            }
        }
        return $totalPrice;
    }
}
