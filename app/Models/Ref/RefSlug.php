<?php

namespace App\Models\Ref;

use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class RefSlug extends Model
{
    protected $table = 'ref_slugs';

    protected $fillable = [
        'slug',
        'model_type',
        'model_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];


    public static function boot()
    {
        parent::boot();

        RefSlug::saved(function ($model) {
            if ($model->getOriginal('slug')) Cache::forget($model->getOriginal('slug').SLUG_MODEL_CACHE);
        });

        RefSlug::updated(function ($model) {
            if ($model->getOriginal('slug')) Cache::forget($model->getOriginal('slug').SLUG_MODEL_CACHE);
        });

        RefSlug::deleted(function ($model) {
            if ($model->getOriginal('slug')) Cache::forget($model->getOriginal('slug').SLUG_MODEL_CACHE);
        });
    }

    public static function getModelBySlug(string $slug): ?RefSlug
    {
        $name = $slug.SLUG_MODEL_CACHE;
        if (!Cache::has($name)) {
            Cache::forever($name, RefSlug::where('slug', 'LIKE', $slug)->first());
        }
        return Cache::get($name);
    }
}
