<?php

namespace App\Models\Ref;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class RefLanguage extends Model
{
    use SoftDeletes;

    const LANG_RU = 'ru';
    const LANG_UK = 'uk';
    const LANG_EN = 'en';

    protected $table = 'ref_languages';
    protected $primaryKey = 'locale';
    protected $keyType = 'string';

    protected $fillable = [
        'name',
        'locale',
        'short_name',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public static function getLanguageWithoutActive(): Collection
    {
        $name = LANGUAGE_WITHOUT_ACTIVE_CACHE . app()->getLocale();
        if (!Cache::has($name)) {
            Cache::forever($name, RefLanguage::where('locale', '!=', app()->getLocale())->get());
        }
        return Cache::get($name);
    }
}
