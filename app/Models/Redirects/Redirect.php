<?php

namespace App\Models\Redirects;

use App\Models\Ref\RefSlug;
use App\Traits\Models\HasSlugTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Redirect extends Model
{
    use HasFactory;
    use HasSlugTrait;

    protected $fillable = [
        'slug_id',
        'redirect_to',
    ];

    protected $appends = [
        'slug',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function slug(): BelongsTo
    {
        return $this->belongsTo(RefSlug::class);
    }
}
