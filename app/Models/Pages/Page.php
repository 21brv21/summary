<?php

namespace App\Models\Pages;

use App\Services\Crud\CrudTranslationTrait;
use App\Traits\Models\HasSlugTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Page  extends Model
{
    use CrudTranslationTrait;
    use HasSlugTrait;

    protected $table = 'ref_pages';
    public string $translationForeignKey = 'ref_page_id';

    protected $fillable = [
        'slug_id',
        'template',
    ];

    protected $appends = [
        'slug',
    ];


    public static function getTemplates()
    {
        $result = [];
        $files = scandir(resource_path() . '/views/web/pages');
        foreach ($files as $key => $file) {
            if ($file == '.' || $file == '..') {
                unset($files[$key]);
            } else {
                $name = str_replace('.blade.php', '', $file);
                $result[$name] = $name;
            }
        }
        return $result;
    }

    public static function getAllPages(): Collection
    {
        $name = PAGE_ALL_CACHE . app()->getLocale();
        if (!Cache::has($name)) {
            Cache::forever($name, Page::with('translation')->get()->sortBy('translation.name'));
        }
        return Cache::get($name);
    }

    public static function getPageBySlugId(int $id)
    {
        $name = $id . PAGE_BY_ID_CACHE . app()->getLocale();
        if (!Cache::has($name)) {
            Cache::forever($name, Page::where('id', $id)->with('translation')->first());
        }
        return Cache::get($name);
    }
}
