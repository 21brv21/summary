<?php

namespace App\Models\Pages;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $table = 'ref_pages_translations';

    protected $fillable = [
        'locale',
        'ref_page_id',
        'name',
        'description',
        'seo_title',
        'seo_description',
        'options',
    ];

    protected $casts = [
        'options' => 'array',
    ];
}
