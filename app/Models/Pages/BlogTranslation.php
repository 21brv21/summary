<?php

namespace App\Models\Pages;

use Illuminate\Database\Eloquent\Model;

class BlogTranslation extends Model
{
    protected $table = 'blog_translations';

    protected $fillable = [
        'locale',
        'blog_id',
        'name',
        'short_description',
        'content',
        'seo_title',
        'seo_description',
    ];
}
