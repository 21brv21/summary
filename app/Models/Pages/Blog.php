<?php

namespace App\Models\Pages;

use App\Services\Crud\CrudTranslationTrait;
use App\Traits\Models\HasImageTrait;
use App\Traits\Models\HasSlugTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Blog extends Model
{
    use CrudTranslationTrait;
    use HasSlugTrait;
    use HasImageTrait;

    protected $table = 'blog';
    public string $translationForeignKey = 'blog_id';

    protected $fillable = [
        'slug_id',
        'image',
    ];

    protected $appends = [
        'slug',
        'date_show',
    ];


    public function getDateShowAttribute(): string
    {
        return Carbon::parse($this->attributes['created_at'])->format('d M Y');
    }

    public static function getLastNews()
    {
        $name = BLOG_LATEST_CACHE . app()->getLocale();
        if (!Cache::has($name)) {
            Cache::forever($name, Blog::with('translation')->orderBy('created_at', "DESC")->limit(3)->get());
        }
        return Cache::get($name);
    }

    public static function getBlogBySlugId(int $id)
    {
        $name = $id.BLOG_BY_ID_CACHE . app()->getLocale();
        if (!Cache::has($name)) {
            Cache::forever($name, Blog::where('id', $id)->with('translation')->first());
        }
        return Cache::get($name);
    }
}
