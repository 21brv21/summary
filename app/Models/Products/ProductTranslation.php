<?php


namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    protected $table = 'products_translations';

    protected $fillable = [
        'locale',
        'product_id',
        'name',
        'options',
        'description',
        'seo_title',
        'seo_description',
        'short_description',
    ];

    protected $casts = [
        'options' => 'array',
    ];
}
