<?php

namespace App\Models\Products;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable = [
        'product_id',
        'name',
        'rating',
        'description',
        'status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];


    protected $appends = [
        'date',
    ];



    public function getDateAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->format('d-m-Y');
    }

}
