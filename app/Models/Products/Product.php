<?php

namespace App\Models\Products;

use App\Models\Files\Image;
use App\Models\Ref\RefSlug;
use App\Services\Crud\CrudTranslationTrait;
use App\Traits\Models\HasImageTrait;
use App\Traits\Models\HasSlugTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use SoftDeletes;
    use CrudTranslationTrait;
    use HasSlugTrait;
    use HasImageTrait;

    public string $translationForeignKey = 'product_id';

    protected $fillable = [
        'price',
        'slug_id',
        'image',
        'image_white',
    ];

    protected $appends = [
        'slug',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function slug(): BelongsTo
    {
        return $this->belongsTo(RefSlug::class);
    }

    public function image(): BelongsTo
    {
        return $this->belongsTo(Image::class);
    }

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function getImageAttribute($value)
    {
        return !is_null($value) ? Storage::disk('public')->url($value) : asset('images/admin/default/image.jpeg');
    }

    public function getNameAttribute()
    {
        $name = $this->attributes['id'] . PRODUCT_NAME_CACHE . app()->getLocale();
        try {
            if (!Cache::has($name)) {
                Cache::forever($name, $this->translation()->first()->name);
            }
        } catch (\Exception) {
            Log::channel('cache')->error('Exception ProductName '.$this->attributes['id']);
        }
        return Cache::get($name);
    }

    public function getImageWhiteAttribute($value): ?string
    {
        return !is_null($value) ? Storage::disk('public')->url($value) : null;
    }
}
