<?php

namespace App\Models\Products;

use App\Models\Exhibit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class ProductImage extends Model
{
    protected $table = 'products_images';
    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'name',
        'path',
        'mime_type',
        'size',
    ];

    protected $appends = [
        'url',
    ];

    public static function boot(): void
    {
        parent::boot();

        self::d(function ($model) {
            $model->updater_id = $model->updater_id ?? auth()->user()->id ?? null;
        });
    }

    public function getUrlAttribute()
    {
        return Storage::disk('public')->url($this->attributes['path']);
    }
}
