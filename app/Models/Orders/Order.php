<?php

namespace App\Models\Orders;

use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'name',
        'phone',
        'bottle_quantity',
        'bottle_price',
        'outpost_quantity',
        'outpost_price',
        'total_price',
        'address',
        'is_new_user',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($order) {
            $order->uuid = Str::uuid()->toString();
        });
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)->withPivot('price', 'quantity');
    }

    public function getTotalPrice(): float
    {
        $total = $this->bottle_quantity * $this->bottle_price;

        foreach ($this->products as $product) {
            if ($product->pivot->quantity > 0) {
                $total += $product->pivot->quantity * $product->pivot->price;
            }
        }

        return $total;
    }

    public function setUuidAttribute()
    {
        $this->attributes['uuid'] = Str::uuid()->toString();
    }
}
