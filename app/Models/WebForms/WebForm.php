<?php

namespace App\Models\WebForms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebForm extends Model
{
    use SoftDeletes;

    protected $table = 'web_forms';
    protected $fillable = [
        'name',
        'phone',
        'theme',
        'message',
    ];

    protected $casts = [
        'content' => 'array',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
