<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class Customer extends User
{
    protected $table = 'users';

    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone',
        'full_name_recipient',
        'address_recipient',
        'phone_recipient',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    protected $attributes = [
        'role' => self::CUSTOMER_ROLE,
    ];


    public function setPhoneRecipientAttribute($value) {
        return $this->attributes['phone_recipient'] = formattedPhone($value);
    }


    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('role', User::CUSTOMER_ROLE);
        });
    }

    public static function getAllClients(): Collection
    {
        if (!Cache::has('ClientsAll')) {
            Cache::forever('ClientsAll', Customer::all());
        }
        return  Cache::get('ClientsAll');
    }

}
