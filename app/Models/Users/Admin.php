<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class Admin extends User
{
    protected $table = 'users';

    protected $attributes = [
        'role' => self::ADMIN_ROLE,
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('role', User::ADMIN_ROLE);
        });
    }

    public static function getAllAdmins(): Collection
    {
        if (!Cache::has(ADMINS_ALL_CACHE)) {
            Cache::forever(ADMINS_ALL_CACHE, Admin::all());
        }
        return  Cache::get(ADMINS_ALL_CACHE);
    }

    public static function getAllAdminsNotificationPhone()
    {
        return  Admin::where('is_phone_notification', true)->get();
    }
}
