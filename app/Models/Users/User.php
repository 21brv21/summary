<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Query\Builder;

/**
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder create(array $attributes = [])
 * @method public Builder update(array $values)
 */

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    const ADMIN_ROLE = 1;
    const CUSTOMER_ROLE = 2;

    protected $fillable = [
        'role',
        'name',
        'surname',
        'email',
        'phone',
        'password',
        'is_phone_notification',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'full_name',
    ];


    public function getFullNameAttribute()
    {
        return $this->attributes['name'].' '.$this->attributes['surname'];
    }

    public function setEmailAttribute($value) {
        return $this->attributes['email'] = empty($value) ? NULL : $value;
    }

    public function setPhoneAttribute($value) {
        return $this->attributes['phone'] = formattedPhone($value);
    }

    public static function getUserByPhone(string $phone) {
        return User::where('phone', $phone)->first();
    }

}
