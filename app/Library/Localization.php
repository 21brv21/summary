<?php

namespace App\Library;

use App\Models\Ref\RefLanguage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Redirect;

class Localization
{
    const MAIN_LOCALE = 'uk';

    public static function getPrefix(): string
    {
        return app()->getLocale() == self::MAIN_LOCALE ? '/' : app()->getLocale();
    }

    public static function configPrefix(): string
    {
        $locale = request()->segment(1);
        in_array($locale, config('app.locales')) ? app()->setLocale($locale) : app()->setLocale(config('app.locale'));

        return self::getPrefix();
    }

    public static function translateSite(string $lang)
    {
        $main = $lang == self::MAIN_LOCALE ? config('app.url') : config('app.url').'/'.$lang;
        $previous = url()->previous();

        $items = array_values(array_filter(explode('/', str_replace(config('app.url'), '', $previous))));

        foreach ($items as $key => $item)
        {
            if (in_array($item, explode('/', $main))) unset($items[$key]);
            if ($item == RefLanguage::LANG_RU) unset($items[$key]);
            if ($item == RefLanguage::LANG_UK) unset($items[$key]);
            if ($item == RefLanguage::LANG_EN) unset($items[$key]);
        }

        $lang = $lang == self::MAIN_LOCALE ? '/' : $lang;

        $url = str_replace('//','/',  $lang.'/'.implode('/', $items));

        return redirect()->to($url);
    }
}
