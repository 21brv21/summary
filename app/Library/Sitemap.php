<?php

namespace App\Library;

use App\Models\Pages\Blog;
use App\Models\Pages\Page;
use App\Models\Products\Product;
use App\Models\Ref\RefSlug;
use Carbon\Carbon;
use stdClass;

class Sitemap
{
    private const BLOG_PRIORITY = 0.8;
    private const PRODUCT_PRIORITY = 0.7;
    private const PAGE_PRIORITY = 0.6;

    public function getSitemap(): array
    {
        $pages = $this->getSitemapPages();
        $products = $this->generateProducts();
        $blog = $this->getSitemapBlog();

        return $pages + $products + $blog;
    }

    public function generateProducts(): array
    {
        return $this->multilangSitemap($this->generateSitemapProducts());
    }

    public function getSitemapPages(): array
    {
        return $this->multilangSitemap($this->generateSitemapPages());
    }

    public function getSitemapBlog(): array
    {
        return $this->multilangSitemap($this->generateSitemapBlog());
    }


    public function generateSitemapProducts(): array
    {
        $rows = [];
        $slugs = RefSlug::where('model_type', Product::class)->get();
        foreach ($slugs as $slug) {
            $row = $this->productLogic($slug);
            if (empty($row)) continue;
            $rows [$row->url] = $row;
        }
        return $rows;
    }

    public function generateSitemapPages(): array
    {
        $rows = [];
        $slugs = RefSlug::where('model_type', Page::class)->get();
        foreach ($slugs as $slug) {
            $row = $this->pageLogic($slug);
            if (empty($row)) continue;
            $rows [$row->url] = $row;
        }
        return $rows;
    }

    public function generateSitemapBlog(): array
    {
        $rows = [];
        $slugs = RefSlug::where('model_type', Blog::class)->get();
        foreach ($slugs as $slug) {
            $row = $this->blogLogic($slug);
            if (empty($row)) continue;
            $rows [$row->url] = $row;
        }
        return $rows;
    }


    //--------PRIVATE--------

    private function add(string $slug, string $lastmod, string $changefreq, $priority): stdClass
    {
        $new = new \stdClass();
        $new->url = $slug;
        $new->lastmod = Carbon::parse($lastmod)->toAtomString();
        $new->changefreq = $changefreq;
        $new->priority = $priority;
        return $new;
    }

    private function multilangSitemap(array $sitemap): array
    {
        $result = [];
        $languages = getLanguages();
        foreach ($languages as $language) {
            foreach ($sitemap as $row) {
                $url = Localization::MAIN_LOCALE == $language->locale ? '/' . $row->url : '/' . $language->locale . '/' . $row->url;

                if ($url == '/') $url = '';
                if ($url == '/ru/') $url = '/ru';
                if ($url == '/uk/') $url = '/uk';
                if ($url == '/en/') {
                    $url = '/en';
                };

                $new = new stdClass();
                $new->url = config('app.url') . $url;
                $new->lastmod = $row->lastmod;
                $new->changefreq = $row->changefreq;
                $new->priority = $row->priority;
                $result[] = $new;
            }
        }
        return $result;
    }

    //PRIVATE function

    private function productLogic(RefSlug $slug): ?stdClass
    {
        $product = Product::where('id', $slug->model_id)->first();
        if (empty($product)) return null;
        return $this->add($slug->slug, $product->updated_at, 'daily', self::PRODUCT_PRIORITY);
    }

    private function pageLogic(RefSlug $slug): ?stdClass
    {
        $page = Page::getPageBySlugId($slug->model_id);
        if (empty($page)) return null;
        return $this->add($slug->slug, $page->updated_at, 'monthly', $slug->slug == '' ? '1.0' : self::PAGE_PRIORITY);
    }

    private function blogLogic(RefSlug $slug): ?stdClass
    {
        $blog = Blog::getBlogBySlugId($slug->model_id);
        if (empty($blog)) return null;
        return $this->add($slug->slug, $blog->updated_at, 'monthly', self::BLOG_PRIORITY);
    }
}
