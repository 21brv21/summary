<?php

namespace App\Library\SeoSchema;

interface InterfaceSchema
{
    public function getSchema();
}
