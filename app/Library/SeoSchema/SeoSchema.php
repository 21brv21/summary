<?php

namespace App\Library\SeoSchema;

use App\Models\Categories\Category;
use App\Models\Pages\Blog;
use App\Models\Pages\Page;
use App\Models\Products\Product;

class SeoSchema
{
    public static function getProductSchemas(Product $product): array
    {
        $breadcrumbSchema = new BreadcrumbSchema($product->name, $product->slug);
        $organizationSchema = new OrganizationSchema();
        $productSchema = new ProductSchema($product);

        $schemas['Organization'] = $organizationSchema->getSchema();
        $schemas['BreadcrumbList'] = $breadcrumbSchema->getSchema();
        $schemas['Product'] = $productSchema->getSchema();

        return $schemas;
    }

    public static function getCategorySchemas(Category $category): array
    {
        $breadcrumbSchema = new BreadcrumbSchema($category->name, $category->slug);
        $organizationSchema = new OrganizationSchema();

        $schemas['Organization'] = $organizationSchema->getSchema();
        $schemas['BreadcrumbList'] = $breadcrumbSchema->getSchema();

        return $schemas;
    }

    public static function getPageSchemas(Page $page): array
    {
        $breadcrumbSchema = new BreadcrumbSchema($page->translation->name, $page->slug);
        $organizationSchema = new OrganizationSchema();

        $schemas['Organization'] = $organizationSchema->getSchema();
        $schemas['BreadcrumbList'] = $breadcrumbSchema->getSchema();

        return $schemas;
    }

    public static function getBlogSchemas(Blog $blog): array
    {
        $breadcrumbSchema = new BreadcrumbSchema($blog->translation->name, $blog->slug);
        $organizationSchema = new OrganizationSchema();

        $schemas['Organization'] = $organizationSchema->getSchema();
        $schemas['BreadcrumbList'] = $breadcrumbSchema->getSchema();

        return $schemas;
    }

    public static function getOrganizationSchemas(): array
    {
        $organizationSchema = new OrganizationSchema();

        $schemas['Organization'] = $organizationSchema->getSchema();

        return $schemas;
    }
}
