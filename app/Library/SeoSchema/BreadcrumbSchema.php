<?php

namespace App\Library\SeoSchema;

use stdClass;

class BreadcrumbSchema implements InterfaceSchema
{
    public string $slug;
    public string $name;

    public function __construct(string $name, string $slug)
    {
        $this->slug = route('web.slug', ['slug' => $slug]);
        $this->name = $name;
    }

    public function getSchema(): string
    {
        $ItemListElement[] = $this->createItemListElement(trans('web.siteName'), config('app.url'), 1);
        $ItemListElement[] = $this->createItemListElement($this->name, $this->slug, 2);

        $schema = new stdClass();
        $schema->{'@context'} = "http://schema.org";
        $schema->{'@type'} = 'BreadcrumbList';
        $schema->ItemListElement =$ItemListElement;

        return json_encode($schema, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    private function createItemListElement(string $name, string $slug, int $position)
    {
        $item = new stdClass();
        $item->{'@type'} = 'ListItem';
        $item->position = $position;
        $item->item = [
            "@id" => $slug,
            "name" => $name
        ];

        return $item;
    }
}
