<?php

namespace App\Library\SeoSchema;

use App\Models\Ref\RefSetting;
use stdClass;

class OrganizationSchema implements InterfaceSchema
{
    public function getSchema(): string
    {
        $schema = new stdClass();
        $schema->{'@context'} = "http://schema.org";
        $schema->{'@type'} = 'Organization';
        $schema->brand = config('app.name');
        $schema->logo = asset('images/web/logo/icon.ico');
        $schema->name = config('app.name');
        $schema->alternateName = "Водолій";
        $schema->url = 'https://vodoliy.kiev.ua';
        $schema->sameAs = $this->getSameAs();
        $schema->department = $this->getDepartment();

        return json_encode($schema, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    private function getSameAs(): array
    {
        return [
            RefSetting::getSettingByKey(RefSetting::SETTING_FACEBOOK)
        ];
    }

    private function getDepartment(): stdClass
    {
        $address = new stdClass();
        $address->{'@type'} = 'PostalAddress';
        $address->addressLocality = "Київ";
        $address->streetAddress = "вул. Васильківська, 25/17";

        $organization = new stdClass();
        $organization->{'@context'} = "http://schema.org";
        $organization->{'@type'} = 'Organization';
        $organization->image = asset('images/web/logo/icon.svg');
        $organization->telephone = [
            RefSetting::getSettingByKey(RefSetting::SETTING_PHONE_KIEVSTAR),
            RefSetting::getSettingByKey(RefSetting::SETTING_PHONE_VODAFONE),
            RefSetting::getSettingByKey(RefSetting::SETTING_PHONE_LIFE),
            RefSetting::getSettingByKey(RefSetting::SETTING_PHONE_CITY),
        ];
        $organization->name = "Водолей";
        $organization->address = $address;

        return $organization;
    }
}
