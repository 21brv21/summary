<?php

namespace App\Library\SeoSchema;

use App\Models\Products\Product;
use Carbon\Carbon;
use stdClass;

class ProductSchema implements InterfaceSchema
{
    public Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getSchema(): string
    {
        $schema = new stdClass();
        $schema->{'@context'} = 'http://schema.org';
        $schema->{'@type'} = 'Product';
        $schema->url = route('web.slug', ['slug' => $this->product->slug]);
        $schema->name = $this->product->name;
        $schema->image = [$this->product->image ?? asset('images/web/logo/logo.webp')];
        $schema->description = $this->product->translation->seo_description;
        $schema->itemCondition = 'https://schema.org/NewCondition';
        $schema->offers = $this->getOffers();
        $schema->brand = $this->getBrand();
        $schema->review = $this->getReview();

        if (count($this->product->reviews) > 0){
            $schema->aggregateRating = $this->getAggregateRating();
        }

        return json_encode($schema, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    private function getOffers()
    {
        $offers = new stdClass();
        $offers->{'@type'} = 'Offer';
        $offers->availability = 'http://schema.org/InStock';
        $offers->url = route('web.slug', ['slug' => $this->product->slug]);
        $offers->price = $this->product->price;
        $offers->priceCurrency = 'UAH';
        $offers->priceValidUntil = now()->addYear()->format('Y-m-d\TH:i:s');
        return $offers;
    }

    private function getBrand()
    {
        $brand = new stdClass();
        $brand->{'@type'} = 'Brand';
        $brand->name = "Водолій";
        $brand->url = route('web.home');

        return $brand;
    }

    private function getAggregateRating()
    {
        $quantity = count($this->product->reviews);
        $ratingValue = 0;
        $bestRating = 0;
        $worstRating = 0;

        foreach ($this->product->reviews as $review) {
            if ($review->rating == 5) $bestRating++;
            if ($review->rating == 1) $worstRating++;
            $ratingValue += $review->rating;
        }

        $ratingValue = $quantity == 0 ? 0 : round($ratingValue / $quantity, 2);

        $aggregateRating = new stdClass();
        $aggregateRating->{'@type'} = 'AggregateRating';
        $aggregateRating->ratingValue = $ratingValue;
        $aggregateRating->ratingCount = $quantity;
        $aggregateRating->bestRating = 5;
        $aggregateRating->worstRating = 1;

        return $aggregateRating;
    }

    private function getReview()
    {
        $result = [];
        foreach ($this->product->reviews as $review) {
            $rating = new stdClass();
            $rating->{'@type'} = 'Rating';
            $rating->ratingValue = $review->rating;

            $author = new stdClass();
            $author->{'@type'} = 'Person';
            $author->name = $review->name;;


            $review = new stdClass();
            $review->{'@type'} = 'Review';
            $review->reviewRating = $rating;
            $review->author = $author;

            $result[] = $review;
        }
        return $result;
    }
}
