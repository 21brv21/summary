<?php

namespace App\Observers;

use App\Jobs\SendToTelegram;
use App\Models\Orders\ContactForm;

class ContactFormObserver
{
    public function created(ContactForm $contactForm): void
    {
        SendToTelegram::dispatch($this->generateTelegramMessage($contactForm));
//        SendToSms::dispatch($form);
    }

    private function generateTelegramMessage(ContactForm $contactForm): string
    {
        return "📞️ Заявка з сайту \n"
            . "Імя: " . $contactForm->name . "\n"
            . "Телефон: " . formattedPhone($contactForm->phone) . "\n";
    }
}
