<?php

namespace App\Jobs;

use App\Models\Users\Admin;
use App\Models\WebForms\WebForm;
use App\Services\Sms\SmsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendToSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public string $message;
    public string $phone;

    public function __construct(string $message, string $phone)
    {
        $this->message = $message;
        $this->phone = $phone;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        SmsService::make()->sendSms($this->webForm->phone, trans('web.success.callbackSms'));
//
//        $admins = Admin::getAllAdminsNotificationPhone();
//        $message = 'Заявка: '.$this->webForm->name.' Телефон: '.formattedPhone($this->webForm->phone);
//
//        foreach ($admins as $admin){
//            SmsService::make()->sendSms($admin->phone,$message);
//        }

    }
}
