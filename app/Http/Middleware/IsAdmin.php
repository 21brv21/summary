<?php

namespace App\Http\Middleware;

use App\Models\Users\User;
use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user() && $request->user()->role == User::ADMIN_ROLE) {
            if ($request->session()->has('redirectTo')) {
                $url = $request->session()->get('redirectTo');
                $request->session()->forget('redirectTo');
                return redirect($url);
            }

            return $next($request);
        }
        $request->session()->put('redirectTo', $request->getRequestUri());
        return redirect(route('login'));
    }
}
