<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;

class CheckPublicInUrl
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $url = $request->url();

        if (Str::contains($url, 'public')) {
            $newUrl = Str::replaceFirst('public', '', $url);
            $newUrl = parse_url($newUrl, PHP_URL_PATH);
            $newUrl = Str::replaceFirst('//', '', $newUrl);
            return redirect(config('app.url').'/'.$newUrl);
        }
        return $next($request);
    }
}
