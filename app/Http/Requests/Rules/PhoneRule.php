<?php

namespace App\Http\Requests\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneRule implements Rule
{
    private const OPERATORS = [
        //Киевстар
        '039',
        '067',
        '068',
        '096',
        '097',
        '098',
        //Vodafone
        '050',
        '066',
        '095',
        '099',
        //Life
        '063',
        '073',
        '093',
        //UTEL
        '091',
        //PEOPLEnet Украина
        '092',
        //Интертелеком Украина
        '089',
        '094',
    ];

    public function passes($attribute, $value): bool
    {
        $phone = formattedPhone($value);
        if (strlen($phone) !== 12) return false;

        return in_array(substr($phone, 2, 3), self::OPERATORS);
    }

    public function message()
    {
        return 'Not correct phone';
    }
}
