<?php

namespace App\Http\Requests\Api\Orders;

use App\Http\Requests\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class ApiContactFormSaveRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'phone' => ['required', new PhoneRule()],
        ];
    }

    public function messages(): array
    {
        return [
            'nameUser.*' => trans('validation.name'),
            'phone.*' => trans('validation.phone'),
        ];
    }
}
