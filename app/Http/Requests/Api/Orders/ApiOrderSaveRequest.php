<?php

namespace App\Http\Requests\Api\Orders;

use App\Http\Requests\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ApiOrderSaveRequest extends FormRequest
{

    public function rules(): array
    {
        $rules =  [
            'name' => ['required', 'string'],
            'phone' => ['required', new PhoneRule()],
            'bottle_quantity' => ['integer'],
            'outpost_quantity' => ['nullable', 'integer'],
            'address' => ['nullable', 'string'],
            'products' => ['nullable', 'array'],
            'is_new_user' => ['required', 'boolean'],
            'products.*.price' => ['required', 'numeric'],
            'products.*.quantity' => ['required', 'integer'],
            'products.*.id' => ['required', 'integer'],
        ];

        if (count(request('products')) === 0){
            $rules['bottle_quantity'][] = 'required';
            $rules['bottle_quantity'][] = 'min:2';
        }

        if (request()->get('is_new_user')){
            $rules['phone'][] = Rule::unique('orders','phone');
        }

        return $rules;
    }

    public function messages(): array
    {
        return [
            'name.*' => trans('validation.nameUser'),
            'phone.unique' => trans('validation.phoneUnique'),
            'phone.*' => trans('validation.phone'),
            'bottle_quantity.*' => trans('validation.bottleQuantity'),
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'is_new_user' => filter_var($this->is_new_user, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
        ]);
    }
}
