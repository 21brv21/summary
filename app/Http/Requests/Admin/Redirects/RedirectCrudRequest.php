<?php

namespace App\Http\Requests\Admin\Redirects;

use App\Models\Ref\RefSetting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RedirectCrudRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'redirect_to' => ['required', 'string', 'max:255'],
            'slug' => ['required', 'string', Rule::unique('ref_slugs', 'slug')->ignore(request()->slug_id ?? null)],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'slug' => rtrim($this->slug, '/'),
        ]);
    }
}
