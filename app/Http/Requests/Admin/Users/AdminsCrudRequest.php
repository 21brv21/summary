<?php

namespace App\Http\Requests\Admin\Users;

use App\Http\Requests\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class AdminsCrudRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        $id = request()->id ?? null;
        $rules = [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|max:255|email|unique:users,email',

            'phone' => [
                'required',
                new PhoneRule(),
                'unique:users,phone'
            ],
        ];

        if ($id){
            $rules['email'] = $rules['email'].','.$id;
            $rules['phone'][2] = $rules['phone'][2].','.$id;
        }else{
            $rules['password'] = 'required|string';
        }

        return $rules;
    }

    public function messages()
    {
        return [
//            'translations.*.name.*' => '234234234234',
        ];
    }
}
