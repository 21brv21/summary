<?php

namespace App\Http\Requests\Admin\Users;

use App\Http\Requests\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class ClientsCrudRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        $id = request()->id ?? null;
        $rules = [
            'name' => 'required|string|max:255',

            'phone' => [
                'required',
                new PhoneRule(),
                'unique:users,phone'
            ],
            'surname' => 'nullable|string|max:255',
            'email' => 'nullable|string|max:255|email|unique:users,email',

            'name_delivery' => 'nullable|string|max:255',
            'surname_delivery' => 'nullable|string|max:255',
            'phone_delivery' => [
                'nullable',
                new PhoneRule(),
            ],
            'address_delivery' => 'nullable|string|max:255',
            'company_name' => 'nullable|string|max:255',
            'edrpou' => 'nullable|string|max:8',
        ];

        if ($id){
            $rules['email'] = $rules['email'].','.$id;
            $rules['phone'][2] = $rules['phone'][2].','.$id;
        }else{
            $rules['password'] = 'required|string';
        }

        return $rules;
    }

    public function messages()
    {
        return [
//            'translations.*.name.*' => '234234234234',
        ];
    }
}
