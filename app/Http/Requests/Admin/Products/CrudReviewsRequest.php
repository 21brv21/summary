<?php

namespace App\Http\Requests\Admin\Products;

use Illuminate\Foundation\Http\FormRequest;

class CrudReviewsRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'rating' => 'required|integer',
            'status' => 'required|boolean',
            'description' => 'nullable|string',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
//            'slug.required' => 'Необхідно вказати slug',
        ];
    }
}
