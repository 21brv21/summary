<?php

namespace App\Http\Requests\Admin\Products;

use App\Models\Ref\RefSetting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CrudProductsRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'slug' => ['required', 'string', Rule::unique('ref_slugs', 'slug')->ignore(request()->slug_id ?? null)],
            'price' => ['required', 'regex:/^\d+(\.\d{1,2})?$/'],

            'translations.*.name' => ['required', 'string', 'max:255'],
            'translations.*.description' => ['nullable', 'string'],
            'translations.*.short_description' => ['required', 'string', 'max:255'],
            'translations.*.seo_title' => ['required', 'string', 'max:'.RefSetting::SEO_TITLE_MAX],
            'translations.*.seo_description' => ['nullable', 'string', 'max:'.RefSetting::SEO_DESCRIPTION_MAX],
            'translations.*.options' => ['nullable', 'array'],
            'translations.*.options.*' => ['required', 'required'],
        ];

        if (request()->file('image')){
            $rules['image'] = ['file', 'mimes:jpg,jpeg,png,gif,webp'];
        }

        if (request()->file('image_white')){
            $rules['image_white'] = ['file', 'mimes:jpg,jpeg,png,gif,webp'];
        }

        return $rules;
    }

    public function messages()
    {
        $lang = app()->getLocale();
        return [
            'slug.required' => trans('validation.slug.required'),
            'slug.unique' => trans('validation.slug.unique'),
            'price.*' => trans('validation.price.required'),
            'quantity.*' => trans('validation.quantity.required'),

            'translations.'.$lang.'.name.*' => trans('validation.name.required'),
            'translations.'.$lang.'.consist.*' => trans('validation.consist.required'),
            'translations.'.$lang.'.description.*' => trans('validation.description.required'),
            'translations.'.$lang.'.seo_title.*' => trans('validation.seo_title.required'),
            'translations.'.$lang.'.seo_description.*' => trans('validation.seo_description.required'),

            'filters.*' => trans('validation.filters'),
            'categories.*' => trans('validation.categories'),
            'manufacturer_id.*' => trans('validation.manufacturer'),
        ];
    }
}
