<?php

namespace App\Http\Requests\Admin\Pages;

use App\Models\Ref\RefSetting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CrudBlogRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        $rules = [
            'slug' => ['required', 'string', Rule::unique('ref_slugs', 'slug')->ignore(request()->slug_id ?? null)],
            'translations.*.name' => ['required', 'string', 'max:255'],
            'translations.*.short_description' => ['required', 'string'],
            'translations.*.content' => ['nullable', 'string'],
            'translations.*.seo_title' => ['nullable', 'string', 'max:'.RefSetting::SEO_TITLE_MAX],
            'translations.*.seo_description' => ['nullable', 'string', 'max:'.RefSetting::SEO_DESCRIPTION_MAX],
        ];

        if (request()->file('image')){
            $rules['image'] = ['file', 'mimes:jpg,jpeg,png,gif,webp'];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'translations.*.template.*' => trans('validation.template'),
            'translations.*.slug.*' => trans('validation.slug'),

            'translations.*.name.*' => trans('validation.name'),
            'translations.*.short_description.*' => trans('validation.short_description'),
            'translations.*.content.*' => trans('validation.content'),
            'translations.*.seo_title.*' => trans('validation.seo_title'),
            'translations.*.seo_description.*' => trans('validation.seo_description'),
        ];
    }
}
