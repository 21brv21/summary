<?php

namespace App\Http\Requests\Admin\Ref;

use Illuminate\Foundation\Http\FormRequest;

class RefCrudLanguagesRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:20',
            'locale' => 'required|string|max:2',
        ];

        $locale = request()->get('locale') ?? null;

        if ($locale){
            $rules['locale'] = $rules['locale'].','.$locale;
        }

        return $rules;
    }

    public function messages()
    {
        return [
//            'translations.*.name.*' => '234234234234',
        ];
    }
}
