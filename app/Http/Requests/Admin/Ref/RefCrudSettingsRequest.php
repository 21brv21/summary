<?php

namespace App\Http\Requests\Admin\Ref;

use Illuminate\Foundation\Http\FormRequest;

class RefCrudSettingsRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:100',
            'key' => 'required|string|max:100|unique:ref_settings,key',
            'value' => 'nullable|string|max:100',
        ];

        $id = request()->id ?? null;

        if ($id){
            $rules['key'] = $rules['key'].','.$id;
        }

        return $rules;
    }

    public function messages()
    {
        return [
//            'translations.*.name.*' => '234234234234',
        ];
    }
}
