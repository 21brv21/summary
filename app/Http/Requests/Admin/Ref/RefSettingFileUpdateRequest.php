<?php

namespace App\Http\Requests\Admin\Ref;

use Illuminate\Foundation\Http\FormRequest;

class RefSettingFileUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        return [
            'file' => ['required', 'file', 'mimes:pdf'],
        ];
    }
}
