<?php

namespace App\Http\Requests\Admin\Ref;

use App\Models\Ref\RefSetting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RefCrudPageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'template' => ['required', 'string'],
            'slug' => ['required', 'string', Rule::unique('ref_slugs', 'slug')->ignore(request()->slug_id ?? null)],
            'translations.*.name' => ['required', 'string', 'max:255'],
            'translations.*.content' => ['nullable', 'string'],
            'translations.*.seo_title' => ['nullable', 'string', 'max:' . RefSetting::SEO_TITLE_MAX],
            'translations.*.seo_description' => ['nullable', 'string', 'max:' . RefSetting::SEO_DESCRIPTION_MAX],
            'translations.*.options' => ['nullable', 'array'],
            'translations.*.options.*' => ['required', 'required'],
        ];
    }

    public function messages()
    {
        return [
//            'translations.*.name.*' => '234234234234',
        ];
    }
}
