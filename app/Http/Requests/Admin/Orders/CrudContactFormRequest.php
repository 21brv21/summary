<?php

namespace App\Http\Requests\Admin\Orders;

use App\Http\Requests\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class CrudContactFormRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'phone' => ['required', new PhoneRule()],
        ];
    }

    public function messages(): array
    {
        return [
            'nameUser.*' => trans('validation.nameUser'),
            'phone.*' => trans('validation.phone'),
        ];
    }
}
