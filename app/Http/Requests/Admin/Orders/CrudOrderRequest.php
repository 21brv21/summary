<?php

namespace App\Http\Requests\Admin\Orders;

use App\Http\Requests\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class CrudOrderRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'phone' => ['required', new PhoneRule()],
            'bottle' => ['required', 'integer'],
            'address' => ['nullable', 'string'],
            'products' => ['nullable', 'array'],
            'products.*.price' => ['required', 'numeric'],
            'products.*.quantity' => ['required', 'integer'],
        ];
    }

    public function messages(): array
    {
        return [
            'name.*' => trans('validation.nameUser'),
            'phone.*' => trans('validation.phone'),
        ];
    }
}
