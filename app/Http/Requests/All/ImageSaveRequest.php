<?php

namespace App\Http\Requests\All;

use Illuminate\Foundation\Http\FormRequest;

class ImageSaveRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        return [
            'files.*' => 'required|file|mimes:jpg,jpeg,png,gif',
        ];
    }
}
