<?php

namespace App\Http\Controllers\Api\Orders;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Orders\ApiOrderSaveRequest;
use App\Models\Orders\Order;
use App\Services\Api\Orders\ApiOrderServices;

class ApiOrderController extends Controller
{
    public function __construct(private ApiOrderServices $orderServices){}

    public function store(ApiOrderSaveRequest $request)
    {
        return response()->json([
            'data' => $this->orderServices->store($request->validated()),
            'message' => trans('web.orderForm.success')
        ]);
    }
}
