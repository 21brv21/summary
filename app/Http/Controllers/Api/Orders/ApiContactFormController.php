<?php

namespace App\Http\Controllers\Api\Orders;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Orders\ApiContactFormSaveRequest;
use App\Services\Api\Orders\ApiContactFormService;

class ApiContactFormController extends Controller
{
    public function __construct(private ApiContactFormService $contactFormService){}

    public function send(ApiContactFormSaveRequest $request)
    {
        return response()->json([
            'data' => $this->contactFormService->store($request->validated()),
            'message' => trans('web.orderForm.success')
        ]);
    }
}
