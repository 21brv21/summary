<?php

namespace App\Http\Controllers\Web\Pages;

use App\Http\Controllers\Controller;
use App\Library\Sitemap;

class SitemapController extends Controller
{
    public function sitemapProducts()
    {
        $sitemap = Sitemap::getSitemapProducts();
        return response()->view('web.pages.sitemap', [
            'sitemap' => $sitemap,
        ])->header('Content-Type', 'text/xml');
    }

    public function sitemapCategories()
    {
        $sitemap = Sitemap::getSitemapCategories();
        return response()->view('web.pages.sitemap', [
            'sitemap' => $sitemap,
        ])->header('Content-Type', 'text/xml');
    }

    public function sitemapPages()
    {
        $sitemap = Sitemap::getSitemapPages();
        return response()->view('web.pages.sitemap', [
            'sitemap' => $sitemap,
        ])->header('Content-Type', 'text/xml');
    }

    public function sitemapBlog()
    {
        $sitemap = Sitemap::getSitemapBlog();
        return response()->view('web.pages.sitemap', [
            'sitemap' => $sitemap,
        ])->header('Content-Type', 'text/xml');
    }
}
