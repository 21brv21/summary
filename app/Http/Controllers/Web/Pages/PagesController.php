<?php

namespace App\Http\Controllers\Web\Pages;

use App\Http\Controllers\Controller;
use App\Library\Localization;
use App\Library\SeoSchema\SeoSchema;
use App\Library\Sitemap;
use App\Models\Pages\Blog;
use App\Models\Pages\Page;
use App\Models\Products\Product;
use App\Models\Redirects\Redirect;
use App\Models\Ref\RefSetting;
use App\Models\Ref\RefSlug;
use App\Services\Web\Pages\PagesService;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Storage;

class PagesController extends Controller
{
    public function home(): View
    {
        $slug = RefSlug::getModelBySlug('');
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getOrganizationSchemas();
        $certificate = Storage::disk('public')->url(getSetting(RefSetting::SETTING_CERTIFICATE));
        return view('web.pages.home', compact('seoSchemas', 'page', 'certificate'));
    }

    public function delivery(): View
    {
        $slug = RefSlug::getModelBySlug('delivery');
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getPageSchemas($page);
        return view('web.pages.delivery', compact('page', 'seoSchemas'));
    }

    public function payment(): View
    {
        $slug = RefSlug::getModelBySlug('payment');
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getPageSchemas($page);
        return view('web.pages.payment', compact('page', 'seoSchemas'));
    }

    public function children(): View
    {
        $slug = RefSlug::getModelBySlug('children');
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getPageSchemas($page);
        return view('web.pages.'.$page->template, compact('page', 'seoSchemas'));
    }

    public function blog(): View
    {
        $slug = RefSlug::getModelBySlug('blog');
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getPageSchemas($page);
        $articles = Blog::orderBy('id', 'desc')->simplePaginate(RefSetting::PAGINATION_WEB);
        return view('web.pages.blog', compact('page', 'seoSchemas', 'articles'));
    }

    public function success(): View
    {
        $slug = RefSlug::getModelBySlug('success');
        $page = Page::getPageBySlugId($slug->model_id);
        $seoSchemas = SeoSchema::getPageSchemas($page);
        return view('web.pages.success', compact('page', 'seoSchemas'));
    }

    public function sitemap(Sitemap $sitemap)
    {
        return response()->view('web.pages.sitemap', [
            'sitemap' => $sitemap->getSitemap(),
        ])->header('Content-Type', 'text/xml');
    }

    public function public(string $slug)
    {
        return redirect(route('web.slug', ['slug' => $slug]));
    }

    public function slug(string $slug)
    {
        try {
            return PagesService::make()->slug($slug);
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    public function translate(string $lang)
    {
        try {
            return Localization::translateSite($lang);
        } catch (\Exception $e) {
            return abort(404);
        }
    }
}


