<?php

namespace App\Http\Controllers\Admin\Ref;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Ref\RefSettingFileUpdateRequest;
use App\Models\Ref\RefSetting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Inertia\Response;

class RefSettingFileController extends Controller
{
    public function get(): Response
    {
        return Inertia::render('Admin/Ref/FileCertificate',[
            'path' => Storage::disk('public')->url(RefSetting::getSettingByKey(RefSetting::SETTING_CERTIFICATE)),
        ]);
    }

    public function update(RefSettingFileUpdateRequest $request): string
    {
        $setting = RefSetting::where('key', RefSetting::SETTING_CERTIFICATE)->first();

        if ($setting->value){
            Storage::disk('public')->delete($setting->value);
        }

        $setting->value = $request->file('file')->store('certificate', 'public');
        $setting->save();

        Cache::forget($setting->key.SETTING_BY_KET_CACHE);

        return Storage::disk('public')->url($setting->value);
    }
}
