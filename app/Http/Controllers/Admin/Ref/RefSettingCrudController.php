<?php

namespace App\Http\Controllers\Admin\Ref;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Ref\RefCrudSettingsRequest;
use App\Models\Ref\RefSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class RefSettingCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(RefSetting::class);
        $this->crud->setValidateRequest(RefCrudSettingsRequest::class);
        $this->crud->setRoute('/admin/ref-settings/');
        $this->crud->setEntityNames(trans('admin.setting'), trans('admin.settings'), trans('admin.settings'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $result = parent::store($request);
        Cache::forget($this->crud->entry->key.SETTING_BY_KET_CACHE);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id,$request);
        Cache::forget($this->crud->entry->key.SETTING_BY_KET_CACHE);
        return $result;
    }

    public function destroy($id)
    {
        $key = RefSetting::find($id)->key;
        Cache::forget($key.SETTING_BY_KET_CACHE);
        return parent::destroy($id);
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'name',
            'label' => trans('admin.name'),
        ]);
        $this->crud->createColumn([
            'name' => 'value',
            'label' => trans('admin.value'),
            'sort' => true,
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'label' => trans('admin.name'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'key',
            'label' => trans('admin.key'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'value',
            'class' => 'col-md-6',
            'label' => trans('admin.value'),
            'required' => true,
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'placeholder' => trans('admin.name'),
        ], function () {
            return RefSetting::get()->pluck('name', 'name')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', $value);
        });
    }
}
