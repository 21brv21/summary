<?php

namespace App\Http\Controllers\Admin\Ref;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Ref\RefCrudLanguagesRequest;
use App\Models\Ref\RefLanguage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class RefLanguagesCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(RefLanguage::class);
        $this->crud->setValidateRequest(RefCrudLanguagesRequest::class);
        $this->crud->setRoute('/admin/ref-languages/');
        $this->crud->setEntityNames(trans('admin.language'), trans('admin.languages'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $result = parent::store($request);
        Cache::forget(LANGUAGE_ALL_CACHE);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id,$request);
        Cache::forget(LANGUAGE_ALL_CACHE);
        return $result;
    }

    public function destroy($id)
    {
        $result = parent::destroy($id);
        $this->resetCache($id);
        return $result;
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'name',
            'label' => trans('admin.name'),
        ]);
        $this->crud->createColumn([
            'name' => 'locale',
            'label' => trans('admin.locale'),
            'sort' => true,
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'label' => trans('admin.name'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'locale',
            'label' => trans('admin.locale'),
            'required' => true,
        ]);

    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'placeholder' => trans('admin.name'),
        ], function () {
            return RefLanguage::get()->pluck('name', 'name')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', $value);
        });
    }
}
