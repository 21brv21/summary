<?php

namespace App\Http\Controllers\Admin\Ref;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Ref\RefCrudPageRequest;
use App\Models\Pages\Page;
use App\Services\Crud\CrudField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class RefPagesCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Page::class);
        $this->crud->setValidateRequest(RefCrudPageRequest::class);
        $this->crud->allowTranslation();
        $this->crud->setRoute('/admin/ref-pages/');
        $this->crud->setEntityNames(trans('admin.page'), trans('admin.pages'), trans('admin.pageAdd'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $result = parent::store($request);
        Page::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($this->crud->entry->id);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id,$request);
        Page::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($id);
        return $result;
    }

    public function destroy($id)
    {
        $result = parent::destroy($id);
        Page::deleteSlug(Page::class, $id);
        $this->resetCache($id);
        return $result;
    }

    private function setupColumns()
    {

        $this->crud->createColumn([
            'name' => 'translation.name',
            'label' => trans('admin.name'),
        ]);

        $this->crud->createColumn([
            'name' => 'template',
            'label' => trans('admin.template'),
        ]);
    }


    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => trans('admin.name'),
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_title',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SEO_TEXT,
            'label' => trans('admin.seo_title'),
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_description',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SEO_TEXTAREA,
            'label' => trans('admin.seo_description'),
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'description',
            'label' => trans('admin.description'),
            'type' => CrudField::INPUT_EDITOR,
            'class' => 'col-md-12',
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'slug',
            'class' => 'col-md-6',
            'type' => CrudField::INPUT_SLUG,
            'label' => trans('admin.slug'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'template',
            'class' => 'col-md-6',
            'label' => trans('admin.template'),
            'type' => CrudField::INPUT_ARRAY_SELECT2,
            'values' => Page::getTemplates(),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'options',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_JSON,
            'label' => trans('admin.options'),
            'translations' => true,
        ]);
    }

    private function setupFilters()
    {
        $pages = Page::getAllPages();
        $this->crud->createFilter([
            'name' => 'name',
            'placeholder' => trans('admin.name'),
        ], function () use ($pages) {
            return $pages->pluck('name', 'name')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', $value);
        });
    }

    private function resetCache(int $id)
    {
        $languages = getLanguages();
        foreach ($languages as $language)
        {
            Cache::forget($id.PAGE_BY_ID_CACHE . $language->locale);
            Cache::forget(PAGE_ALL_CACHE . $language->locale);
        }
    }
}
