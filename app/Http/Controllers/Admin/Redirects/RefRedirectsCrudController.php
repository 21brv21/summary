<?php

namespace App\Http\Controllers\Admin\Redirects;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Redirects\RedirectCrudRequest;
use App\Models\Redirects\Redirect;
use Illuminate\Http\Request;

class RefRedirectsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Redirect::class);
        $this->crud->setValidateRequest(RedirectCrudRequest::class);
        $this->crud->setRoute('/admin/redirects/');
        $this->crud->setEntityNames(trans('admin.redirect'), trans('admin.redirects'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $result = parent::store($request);
        Redirect::saveSlug($request->get('slug'), $this->crud->entry);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id,$request);
        Redirect::saveSlug($request->get('slug'), $this->crud->entry);
        return $result;
    }

    public function destroy($id)
    {
        $result = parent::destroy($id);
        Redirect::deleteSlug(Redirect::class, $id);
        return $result;
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'slug',
            'label' => trans('admin.slug'),
        ]);
        $this->crud->createColumn([
            'name' => 'redirect_to',
            'label' => trans('admin.redirect_to'),
            'sort' => true,
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'slug',
            'label' => trans('admin.slug'),
            'required' => true,
            'class' => 'col-md-6',
        ]);
        $this->crud->createField([
            'name' => 'redirect_to',
            'label' => trans('admin.redirect_to'),
            'required' => true,
            'class' => 'col-md-6',
        ]);

    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'redirect_to',
            'placeholder' => trans('admin.name'),
        ], function () {
            return Redirect::get()->pluck('redirect_to', 'redirect_to')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('redirect_to', $value);
        });
    }
}
