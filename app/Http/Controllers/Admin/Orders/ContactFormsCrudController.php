<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Orders\CrudContactFormRequest;
use App\Models\Orders\ContactForm;
use App\Services\Crud\CrudField;

class ContactFormsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(ContactForm::class);
        $this->crud->setValidateRequest(CrudContactFormRequest::class);
        $this->crud->setRoute('/admin/contact-forms/');
        $this->crud->setAllowShow(true);
        $this->crud->setAllowCreat(false);
        $this->crud->setAllowUpdate(false);
        $this->crud->setEntityNames(trans('admin.contactForm'), trans('admin.contactForms'), trans('admin.webFormAdd'));
        $this->crud->query= $this->crud->query->orderBy('created_at', 'DESC');

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'name',
            'label' => trans('admin.name'),
        ]);

        $this->crud->createColumn([
            'name' => 'phone',
            'label' => trans('admin.phone'),
            'sort' => true,
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => trans('admin.firstName'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'phone',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_PHONE,
            'label' => trans('admin.phone'),
            'required' => false,
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'phone',
            'placeholder' => trans('admin.phone'),
        ], function () {
            return ContactForm::get()->pluck('phone', 'phone')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('phone', $value);
        });
    }
}
