<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Users\AdminsCrudRequest;
use App\Models\Ref\RefSetting;
use App\Models\Users\Admin;
use App\Services\Crud\CrudField;
use App\Services\Crud\CrudFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Admin::class);
        $this->crud->setValidateRequest(AdminsCrudRequest::class);
        $this->crud->setRoute('/admin/admins/');
        $this->crud->setEntityNames(trans('admin.admin'), trans('admin.admins'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $password = generatePassword();
        $request->request->add(['password' => Hash::make($password)]);
        return parent::store($request);
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'full_name',
            'label' => trans('admin.fullName'),
        ]);
        $this->crud->createColumn([
            'name' => 'phone',
            'label' => trans('admin.phone'),
            'sort' => true,
        ]);
        $this->crud->createColumn([
            'name' => 'email',
            'label' => trans('admin.email'),
            'sort' => true,
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'label' => trans('admin.firstName'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'surname',
            'label' => trans('admin.surname'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'email',
            'label' => trans('admin.email'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'phone',
            'label' => trans('admin.phone'),
            'required' => true,
            'type' => CrudField::INPUT_PHONE,
        ]);
    }

    private function setupFilters()
    {
        $admins = Admin::all();

        $this->crud->createFilter([
            'name' => 'phone',
            'placeholder' => trans('admin.phone'),
        ], function () use ($admins) {
            return $admins->pluck('phone', 'id')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('phone', $value);
        });
    }

}
