<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Users\ClientsCrudRequest;
use App\Models\Users\Customer;
use App\Services\Crud\CrudField;
use App\Services\Crud\CrudPanel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ClientsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Customer::class);
        $this->crud->setValidateRequest( ClientsCrudRequest::class);
        $this->crud->setRoute('/admin/clients/');
        $this->crud->setEntityNames(trans('admin.addClient'), trans('admin.clients'));


        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $request->request->add(['password' => Hash::make(generatePassword())]);
        return parent::store($request);
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'full_name',
            'label' => trans('admin.fullName'),
        ]);
        $this->crud->createColumn([
            'name' => 'phone',
            'label' => trans('admin.phone'),
            'sort' => true,
        ]);
        $this->crud->createColumn([
            'name' => 'email',
            'label' => trans('admin.email'),
            'sort' => true,
        ]);
    }


    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'label' => trans('admin.firstName'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'surname',
            'label' => trans('admin.surname'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'email',
            'label' => trans('admin.email'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'phone',
            'label' => trans('admin.phone'),
            'required' => true,
            'type' => CrudField::INPUT_PHONE,
        ]);
        $this->crud->createField([
            'name' => 'company_name',
            'label' => trans('admin.company_name'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'edrpou',
            'label' => trans('admin.edrpou'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'name_delivery',
            'label' => trans('admin.name_delivery'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'surname_delivery',
            'label' => trans('admin.surname_delivery'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'phone_delivery',
            'label' => trans('admin.phone_delivery'),
            'required' => true,
        ]);
        $this->crud->createField([
            'name' => 'address_delivery',
            'label' => trans('admin.address_delivery'),
            'required' => true,
        ]);
    }

    //---------FIELDS---------

    private function setupFilters()
    {

        $clients = Customer::all();

        $this->crud->createFilter([
            'name' => 'phone',
            'placeholder' => trans('admin.phone'),
        ], function () use ($clients) {
            return $clients->pluck('phone', 'id')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('phone', $value);
        });
    }
}
