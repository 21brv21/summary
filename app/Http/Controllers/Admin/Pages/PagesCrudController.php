<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Pages\CrudPageRequest;
use App\Models\Pages\Page;
use App\Services\Crud\CrudField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PagesCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Page::class);
        $this->crud->setValidateRequest(CrudPageRequest::class);
        $this->crud->setRoute('/admin/ref-pages/');
        $this->crud->allowTranslation();
        $this->crud->setEntityNames(trans('admin.page'), trans('admin.pages'), trans('admin.pageAdd'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }


    public function store(Request $request)
    {
        $result = parent::store($request);
        Page::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($this->crud->entry->id);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id,$request);
        Page::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($id);
        return $result;
    }

    public function destroy($id)
    {
        $result = parent::destroy($id);
        Page::deleteSlug(Page::class, $id);
        $this->resetCache($id);
        return $result;
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'translation.name',
            'label' => trans('admin.name'),
        ]);
        $this->crud->createColumn([
            'name' => 'slug',
            'label' => trans('admin.slug'),
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => trans('admin.name'),
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_title',
            'class' => 'col-md-12',
            'label' => trans('admin.seo_title'),
            'type' => CrudField::INPUT_SEO_TEXT,
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_description',
            'class' => 'col-md-12',
            'label' => trans('admin.seo_description'),
            'type' => CrudField::INPUT_SEO_TEXTAREA,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'content',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_EDITOR,
            'label' => trans('admin.content'),
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'slug',
            'class' => 'col-md-4',
            'type' => CrudField::INPUT_SLUG,
            'label' => trans('admin.slug'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'template',
            'class' => 'col-md-4',
            'label' => trans('admin.template'),
            'type' => CrudField::INPUT_ARRAY_SELECT2,
            'values' => Page::getTemplates(),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'image',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_IMAGE,
            'label' => trans('admin.image'),
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'placeholder' => trans('admin.name'),
        ], function () {
            return Page::get()->pluck('name', 'name')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', $value);
        });
    }

    private function resetCache(int $id)
    {
        $languages = getLanguages();
        foreach ($languages as $language)
        {
            Cache::forget($id.PAGE_BY_ID_CACHE . $language->locale);
            Cache::forget(PAGE_ALL_CACHE . $language->locale);
        }
    }

}
