<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use App\Models\Users\Customer;
use App\Services\Admin\Pages\DashboardServices;
use Inertia\Inertia;
use stdClass;

class PagesController extends Controller
{
    public function dashboard()
    {
//        $ordersAll = Order::getOrdersAll();

        $data = new stdClass;
//        $data->salesGrph = DashboardServices::make()->getSalesGrph();
//        $data->totalInvoices = $ordersAll->count();
//        $data->ordersPriceInfo = DashboardServices::make()->getOrdersPriceInfo($ordersAll);
//        $data->totalPriceProducts = DashboardServices::make()->getTotalPriceProducts();
//        $data->totalClients = Customer::getAllClients()->count();


        return Inertia::render('Dashboard' ,compact('data'));
    }
}
