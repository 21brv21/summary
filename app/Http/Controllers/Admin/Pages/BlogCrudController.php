<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Pages\CrudBlogRequest;
use App\Models\Pages\Blog;
use App\Services\Crud\CrudField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BlogCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Blog::class);
        $this->crud->setValidateRequest(CrudBlogRequest::class);
        $this->crud->allowTranslation();
        $this->crud->setViewCreate('Blog/Create');
        $this->crud->setViewEdit('Blog/Edit');
        $this->crud->setRoute('/admin/blog/');
        $this->crud->setEntityNames(trans('admin.blog'), trans('admin.blogs'));

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $result = parent::store($request);
        Blog::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($this->crud->entry->id);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id,$request);
        Blog::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($id);
        return $result;
    }

    public function destroy($id)
    {
        $result = parent::destroy($id);
        Blog::deleteSlug(Blog::class, $id);
        $this->resetCache($id);
        return $result;
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'translation.name',
            'label' => trans('admin.name'),
        ]);
        $this->crud->createColumn([
            'name' => 'slug',
            'label' => trans('admin.slug'),
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => trans('admin.name'),
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'short_description',
            'class' => 'col-md-12',
            'label' => trans('admin.short_description'),
            'type' => CrudField::INPUT_TEXTAREA,
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_title',
            'class' => 'col-md-12',
            'label' => trans('admin.seo_title'),
            'type' => CrudField::INPUT_SEO_TEXT,
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_description',
            'class' => 'col-md-12',
            'label' => trans('admin.seo_description'),
            'type' => CrudField::INPUT_SEO_TEXTAREA,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'content',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_EDITOR,
            'label' => trans('admin.content'),
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'slug',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SLUG,
            'label' => trans('admin.slug'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'image',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_IMAGE,
            'label' => trans('admin.image'),
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'placeholder' => trans('admin.name'),
        ], function () {
            return Blog::get()->pluck('name', 'name')->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', $value);
        });
    }

    public function resetCache(int $id):void
    {

        $languages = getLanguages();
        foreach ($languages as $language)
        {
            Cache::forget($id.BLOG_BY_ID_CACHE.$language->locale);
            Cache::forget(BLOG_LATEST_CACHE.$language->locale);
        }
    }
}
