<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Products\CrudProductsRequest;
use App\Models\Products\Product;
use App\Services\Crud\CrudColumn;
use App\Services\Crud\CrudField;
use App\Services\Crud\CrudFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Product::class);
        $this->crud->setValidateRequest(CrudProductsRequest::class);
        $this->crud->setRoute('/admin/products/');
        $this->crud->setEntityNames(trans('admin.product'), trans('admin.productsName'));
        $this->crud->setViewCreate('Products/Create');
        $this->crud->setViewEdit('Products/Edit');
        $this->crud->allowTranslation();
        $this->crud->addRelation('images');

        if (empty(request()->get('orderBy'))) {
            $this->crud->query = $this->crud->query->orderBy('id', 'DESC');
        }

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    public function store(Request $request)
    {
        $result = parent::store($request);
        Product::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($this->crud->entry->id);
        return $result;
    }

    public function update($id, Request $request)
    {
        $result = parent::update($id, $request);
        Product::saveSlug($request->get('slug'), $this->crud->entry);
        $this->resetCache($this->crud->entry->id);
        return $result;
    }

    public function destroy($id)
    {
        $result = parent::destroy($id);
        Product::deleteSlug(Product::class, $id);
        $this->resetCache($id);
        return $result;
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'id',
            'label' => trans('admin.id'),
            'sort' => true,
        ]);

        $this->crud->createColumn([
            'name' => 'translation.name',
            'label' => trans('admin.name'),
            'length' => 40,
        ]);

        $this->crud->createColumn([
            'name' => 'price',
            'label' => 'ГРН',
            'type' => CrudColumn::COLUMN_NUMBER,
            'sort' => true,
            'classes' => 'minW80',
        ]);

        $this->crud->createColumn([
            'name' => 'image',
            'label' => 'Photo',
            'type' => CrudColumn::COLUMN_IMAGE,
            'sort' => true,
            'classes' => 'minW90',
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'slug',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SLUG,
            'label' => trans('admin.slug'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'price',
            'class' => 'col-md-12',
            'label' => trans('admin.price'),
            'type' => CrudField::INPUT_FLOAT,
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'image',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_IMAGE,
            'label' => trans('admin.img_main'),
        ]);

        $this->crud->createField([
            'name' => 'image_white',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_IMAGE,
            'label' => trans('admin.img_main'),
        ]);

        //-----------translations-----------

        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => trans('admin.name'),
            'required' => true,
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'short_description',
            'label' => trans('admin.short_description'),
            'type' => CrudField::INPUT_TEXTAREA,
            'class' => 'col-md-12',
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'description',
            'label' => trans('admin.description'),
            'type' => CrudField::INPUT_EDITOR,
            'class' => 'col-md-12',
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_title',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SEO_TEXT,
            'label' => trans('admin.seo_title'),
            'translations' => true,
        ]);

        $this->crud->createField([
            'name' => 'seo_description',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SEO_TEXTAREA,
            'label' => trans('admin.seo_description'),
            'translations' => true,
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'class' => 'col-md-6',
            'type' => CrudFilter::FILTER_SEARCH_API,
            'placeholder' => trans('admin.nameOrCod'),
        ], function () {
            return [];
        }, function ($value) {
            $this->crud->query = $this->crud->query->where(function ($query) use ($value) {
                $query->whereHas('translations', function (Builder $query) use ($value) {
                    $query->where('name', 'LIKE', '%' . $value . '%');
                })->orWhere('id', 'LIKE', '%' . $value . '%');
            });
        });
    }


    private function resetCache(int $id)
    {
        $languages = getLanguages();
        foreach ($languages as $language) {
            Cache::forget($id . PRODUCT_NAME_CACHE . $language->locale);
            Cache::forget(PRODUCT_ALL_CACHE . $language->locale);
        }
    }
}
