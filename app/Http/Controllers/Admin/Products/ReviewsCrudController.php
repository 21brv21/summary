<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\CrudController;
use App\Http\Requests\Admin\Products\CrudReviewsRequest;
use App\Models\Products\Review;
use App\Services\Crud\CrudColumn;
use App\Services\Crud\CrudField;

class ReviewsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Review::class);
        $this->crud->setValidateRequest(CrudReviewsRequest::class);
        $this->crud->setRoute('/admin/reviews/');
        $this->crud->setEntityNames(trans('admin.review'), trans('admin.reviews'));
        $this->crud->setAllowCreat(false);

        $this->setupColumns();
        $this->setupFields();
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'name',
            'label' => trans('admin.fullName'),
        ]);

        $this->crud->createColumn([
            'name' => 'rating',
            'label' => trans('admin.rating'),
        ]);

        $this->crud->createColumn([
            'name' => 'status',
            'label' => trans('admin.status'),
            'type' => CrudColumn::COLUMN_BOOLEAN,
        ]);
    }


    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'label' => trans('admin.fullName'),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'rating',
            'label' => trans('admin.rating'),
            'type' => CrudField::INPUT_NUMBER,
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'status',
            'label' => trans('admin.status'),
            'type' => CrudField::INPUT_BOOLEAN,
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'description',
            'class' => 'col-md-12',
            'label' => trans('admin.description'),
        ]);

    }
}
