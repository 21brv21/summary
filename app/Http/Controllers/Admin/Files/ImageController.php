<?php

namespace App\Http\Controllers\Admin\Files;

use App\Http\Controllers\Controller;
use App\Http\Requests\All\ImageSaveRequest;
use App\Models\Files\Image;
use App\Services\Admin\Files\ImageService;

class ImageController extends Controller
{
    public function __construct(private ImageService $imageService){}

    public function store(string $modelName, int $modelId, ImageSaveRequest $request,): array
    {
        $model = $this->imageService->getModel($modelName)->find($modelId);

        foreach ($request->file('files') as $file)
        {
            $images[] = $model->images()->create($this->imageService->uploadFile($file, $modelName));
        }

        return $images ?? [];
    }

    public function destory(string $modelName, Image $image): bool
    {
        return $image->delete();
    }

}
