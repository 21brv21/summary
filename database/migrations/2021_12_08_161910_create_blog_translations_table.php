<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Ref\RefSetting;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('blog_id');
            $table->string('locale', 2)->index();
            $table->string('name');
            $table->string('short_description' ,255)->nullable();
            $table->text('content')->nullable();
            $table->string('seo_title',RefSetting::SEO_TITLE_MAX);
            $table->string('seo_description',RefSetting::SEO_DESCRIPTION_MAX)->nullable();
            $table->timestamps();

            $table->foreign('locale')->references('locale')->on('ref_languages')->cascadeOnDelete();
            $table->foreign('blog_id')->references('id')->on('blog')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_translations');
    }
};
