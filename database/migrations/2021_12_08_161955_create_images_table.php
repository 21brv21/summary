<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->morphs('imageable');
            $table->string('name');
            $table->string('path');
            $table->string('mime_type');
            $table->unsignedInteger('size');
        });
    }

    public function down()
    {
        Schema::dropIfExists('images');
    }
}
