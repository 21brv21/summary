<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Ref\RefSetting;

class CreateProductsTranslationsTable extends Migration
{
    public function up()
    {
        Schema::create('products_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->string('locale', 2)->index();
            $table->string('name');
            $table->json('options')->nullable();
            $table->text('description')->nullable();
            $table->string('short_description')->nullable();
            $table->string('seo_title',RefSetting::SEO_TITLE_MAX);
            $table->string('seo_description',RefSetting::SEO_DESCRIPTION_MAX)->nullable();
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('locale')->references('locale')->on('ref_languages');
        });
    }

    public function down()
    {
        Schema::dropIfExists('products_translations');
    }
}
