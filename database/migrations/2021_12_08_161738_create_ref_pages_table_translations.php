<?php

use App\Models\Ref\RefSetting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_pages_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ref_page_id');
            $table->string('locale', 2)->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('seo_title',RefSetting::SEO_TITLE_MAX);
            $table->string('seo_description',RefSetting::SEO_DESCRIPTION_MAX)->nullable();
            $table->json('options')->nullable();
            $table->timestamps();

            $table->foreign('locale')->references('locale')->on('ref_languages');
            $table->foreign('ref_page_id')->references('id')->on('ref_pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_pages_table_translations');
    }
};
