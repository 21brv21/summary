<?php

namespace Database\Seeders;

use App\Models\Pages\Page;
use App\Models\Pages\PageTranslation;
use App\Models\Ref\RefLanguage;
use App\Models\Ref\RefSlug;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = array_map(function($v){return str_getcsv($v, ";");}, file(database_path('data/pages.csv')));
        foreach ($rows as $key => $row) {
            if ($key == 0) continue;

            $slug = RefSlug::getModelBySlug($row[1]);

            if (empty($slug)){
                $page = new Page();
                $page->template = $row[2];
                $page->save();

                Page::saveSlug($row[1], $page);
            }else{
                $page = Page::where('template', $row[2])->where('slug_id', $slug->id)->first();
            }

            $data = [
                'locale' => $row[3],
                'ref_page_id' => $page->id,
                'name' => $row[4],
                'description' => $row[5],
                'seo_title' => $row[6],
                'seo_description' => $row[7],
            ];

            PageTranslation::updateOrCreate($data, $data);

            dump($row[4]);
        }
    }
}
