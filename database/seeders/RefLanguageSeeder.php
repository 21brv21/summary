<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ref\RefLanguage;

class RefLanguageSeeder extends Seeder
{
    public function run()
    {
        $languages = [
            [
                'name' => 'Українська',
                'locale' => 'uk',
                'short_name' => 'UA',
            ],
            [
                'name' => 'Русский',
                'locale' => 'ru',
                'short_name' => 'RU',
            ],
            [
                'name' => 'English',
                'locale' => 'en',
                'short_name' => 'EN',
            ],
        ];

        foreach ($languages as $language)
        {
            $lang = RefLanguage::updateOrCreate(
                ['name' => $language['name'], 'locale' => $language['locale']],
                ['name' => $language['name'], 'locale' => $language['locale']]
            );
            dump($lang->name);
        }
    }
}
