<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        Artisan::call('cache:clear');

        $this->call(AdminSeeder::class);
        $this->call(RefLanguageSeeder::class);
        $this->call(RefSettingsSeeder::class);
        $this->call(PagesSeeder::class);
    }
}
