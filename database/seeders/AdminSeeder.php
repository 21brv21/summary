<?php

namespace Database\Seeders;

use App\Models\Users\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    public function run()
    {
        $user = new User();
        $user->role = User::ADMIN_ROLE;
        $user->name = 'Roman';
        $user->email = 'belous@horizon.in.ua';
        $user->phone = '380503122943';
        $user->password = Hash::make(2121);
        $user->save();
    }
}
