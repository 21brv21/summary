<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ref\RefSetting;

class RefSettingsSeeder extends Seeder
{
    public function run()
    {

        $settings = [
            [
                'name' => 'Номер Lifecell',
                'key' => RefSetting::SETTING_PHONE_LIFE,
                'value' => '+38(073)357-7070',
            ],
            [
                'name' => 'Номер Киевстар',
                'key' => RefSetting::SETTING_PHONE_KIEVSTAR,
                'value' => '+38(098)357-7070',
            ],
            [
                'name' => 'Номер Vodafone',
                'key' => RefSetting::SETTING_PHONE_VODAFONE,
                'value' => '+38(099)357-7070',
            ],
            [
                'name' => 'Номер Київський',
                'key' => RefSetting::SETTING_PHONE_CITY,
                'value' => '+38(044)357-7070',
            ],
            [
                'name' => 'Telegram',
                'key' => RefSetting::SETTING_TELEGRAM,
                'value' => 'https://t.me/vodoliy_voda',
            ],
            [
                'name' => 'Viber',
                'key' => RefSetting::SETTING_VIBER,
                'value' => '________',
            ],
            [
                'name' => 'Facebook',
                'key' => RefSetting::SETTING_FACEBOOK,
                'value' => 'https://www.facebook.com/vodoley.dp.ua',
            ],
            [
                'name' => 'Ціна за бутиль до 2шт',
                'key' => RefSetting::SETTING_PRICE_FIRST,
                'value' => 120,
            ],
            [
                'name' => 'Ціна за бутиль 3-7шт',
                'key' => RefSetting::SETTING_PRICE_SECOND,
                'value' => 110,
            ],
            [
                'name' => 'Ціна за бутиль від 8шт',
                'key' => RefSetting::SETTING_PRICE_THIRD,
                'value' => 100,
            ],
            [
                'name' => 'SALE Ціна за бутиль до 2шт',
                'key' => RefSetting::SETTING_SALE_PRICE_FIRST,
                'value' => 50,
            ],
            [
                'name' => 'SALE Ціна за бутиль 3-7шт',
                'key' => RefSetting::SETTING_SALE_PRICE_SECOND,
                'value' => 110,
            ],
            [
                'name' => 'SALE Ціна за бутиль від 10шт',
                'key' => RefSetting::SETTING_SALE_PRICE_THIRD,
                'value' => 90,
            ],
            [
                'name' => 'Заставна вартість',
                'key' => RefSetting::SETTING_OUTPOST_PRICE,
                'value' => 300,
            ],
            [
                'name' => 'Протокол випробування',
                'key' => RefSetting::SETTING_CERTIFICATE,
                'value' => 'certificate/USvOqyeLtUEVAKUijNfpb0EKW52FdC4bqHldseOt.pdf',
            ],
            [
                'name' => 'ІПН',
                'key' => RefSetting::SETTING_PAYMENT_IPN,
                'value' => '3258014292',
            ],
            [
                'name' => 'ІПН',
                'key' => RefSetting::SETTING_PAYMENT_RR,
                'value' => 'UA663052990000026008036213888',
            ],
            [
                'name' => 'р/р',
                'key' => RefSetting::SETTING_PAYMENT_RR,
                'value' => 'UA663052990000026008036213888',
            ],
            [
                'name' => 'Банк',
                'key' => RefSetting::SETTING_PAYMENT_BANK,
                'value' => 'в ПАТ КБ «Приватбанк»',
            ],
            [
                'name' => 'МФО',
                'key' => RefSetting::SETTING_PAYMENT_MFO,
                'value' => '300711',
            ],
            [
                'name' => 'Юр назва',
                'key' => RefSetting::SETTING_PAYMENT_NAME,
                'value' => 'Фізична особа – підприємець Ус Сергій Іванович',
            ],
        ];

        foreach ($settings as $setting)
        {
            RefSetting::updateOrCreate(
                ['name' => $setting['name'], 'key' => $setting['key'], 'value' => $setting['value']],
                ['name' => $setting['name'], 'key' => $setting['key'], 'value' => $setting['value']]
            );

            dump($setting['name']);
        }
    }
}
