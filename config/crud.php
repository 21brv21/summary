<?php

return [
    'name' => env('APP_NAME', 'Laravel'),
    'translation_suffix' => 'Translation',
];
